@extends('layouts.app')

@section('content')

<style>

.select2-container--material {
    width: 100% !important;
    
    ::placeholder {
        color: inherit;
}
    
</style>

<div class="content">
    <div class="container-fluid">

        <div class="main-card mb-3 card">
        
            <div class="card-header card-header-warning">
                <h3 class="card-title">Daftar Ibadah</h3>
            </div>

            <div class="card-body">

                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah User</a>
                {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
                <br/><br/>
                <div class="overflow-auto">
                    <table class="table table-striped table-hover" id="ibadah-table" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="max-width: 100px;"><strong>No</strong></th>
                                <th ><strong>Nama</strong></th>
                                <th ><strong>Target</strong></th>
                                <th ><strong>Kode</strong></th>
                                <th ><strong>Icon</strong></th>
                                <th ><strong></strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@include('ortu.modal_add_ortu')
@include('ortu.modal_edit_ortu')

<script>
$(function() {

    var table = $('#ibadah-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('get_ibadahs') !!}",
            data: {
                
            },
        },
        columns: [
            { data: 'no', name: 'no' , searchable: true},
            { data: 'nama', name: 'nama' , searchable: true},
            { data: null, name: 'target', render: function ( data, type, row ) {
                return data.target + ' ' + data.satuan;
            } },
            { data: 'kode', name: 'email' , searchable: true},
            { data: null, name: 'icon', render: function ( data, type, row ) {
                return '<img src="{{ asset('images') }}/icon/' + data.icon + '" style="height: 50px; "/>';
            } },
            { data: null, name: 'action', render: function ( data, type, row ) {
                return '<a href="#" title="Edit Data OTS" data-toggle="modal" data-target="#modal_edit" style="padding: 10px;" onclick="set_edit(' + data.id + ')"><i class="material-icons">create</i></a>' + 
                '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick="confirmDel(' + data.id + ', \'' + data.nama + '\')" title="Hapus"><i class="material-icons">delete</i></a>' +
                '<form class="delete" action="{{route('delete_ortu')}}" method="post">@csrf' + 
                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                '</form>' +
                '</div>';
            } },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "searchable": false,
            "orderable": false,
            "targets": 0
            }]
    });
    
});

function confirmDel(id, name) {
    var txt;
    var r = confirm("Yakin akan menghapus data OTS? \n\nNama OTS: " + name);
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#delButton'+id).click();
    } else {
        txt = "You pressed Cancel!";
    }
}

function confirmReset(id, name) {
    var txt;
    var r = confirm("Yakin akan reset password OTS? \n\nNama OTS: " + name + "\nPassword baru: 12345678");
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#resetForm').submit();
    } else {
        txt = "You pressed Cancel!";
    }
}

$(document).ready(function domReady() {
    $("#id_ortu").select2({
        placeholder: "Pilih Orang Tua Siswa",
        theme: "material",
        dropdownParent: $("#modal_add")
    });

    $("#id_ortu_edit").select2({
        placeholder: "Pilih Orang Tua Siswa",
        theme: "material",
        dropdownParent: $("#modal_edit")
    });
    
    // $(".select2-selection__arrow")
    //     .addClass("material-icons")
    //     .html("arrow_drop_down");
});

</script>
@stack('scripts')

@endsection
