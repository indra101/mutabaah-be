<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Buku Baru</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{-- <form class="form-horizontal" method="POST" action="{{ route('add_buku') }}"> --}}
            {{ Form::open(array('url' => '/add_buku','files'=>'true')) }}
  
            {{ csrf_field() }}
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Judul Buku</label>
                          {{ Form::text('judul', '', array('class' => 'form-control pl-2')) }}
  
                          @if ($errors->has('judul'))
                            <span class="help-block text-danger">
                                <small>Judul Buku belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kelas</label>
                          {{ Form::select('id_level', $levels, 0, array('class' => 'form-control pl-2', 'id' => 'id_level', 'onchange' => 'get_sub_level()')) }}
  
                          @if ($errors->has('id_level'))
                            <span class="help-block text-danger">
                                <small>Kelas belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Sub Kelas</label>
                          {{ Form::select('id_sub_level', array('' => 'Pilih Sub Level'), 0, array('class' => 'form-control pl-2', 'id' => 'id_sub_level')) }}
  
                          @if ($errors->has('id_sub_level'))
                            <span class="help-block text-danger">
                                <small>Sub Kelas belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                    <div class="col-md-12">
                      <div>
                        <label class="bmd-label-floating">Upload File</label>
                        <div>
                        {{ Form::file('file_upload') }}
  
                        <br>
                        @if ($errors->has('file_upload'))
                          <span class="help-block text-danger">
                              <small>{{ $errors->first('file_upload') }}</small>
                          </span>
                        @endif
                        </div>
                      </div>
                    </div>
                </div>
  
                    <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
      $('#btnSimpanAdd').hide()
      $('#btnLoadSimpanAdd').show()
      return true;
  } else {
      return false;
  }
});

function get_sub_level() {
    var id_level = $('#id_level').val();

    $.ajax({
        url: '{{ route("get_sub_levels_buku") }}',
        data: 'id_level=' + id_level,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level').empty();
            $('#id_sub_level').append('<option value="">Pilih Rombel</option>');
            $.each(data, function(key, value) {
                $('#id_sub_level').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

</script>