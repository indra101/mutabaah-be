@extends('layouts.app')

@section('content')

<style>

.card-profile .card-avatar {
    max-width: 230px;
    max-height: 230px;
}

</style>

<div class="content">
  <div class="container-fluid pt-5">

    <div class="row">

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/muslimah.png" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">Mrs. Diani Wijayanti</h4>
              <h5 class="card-title"><i class="material-icons">phone_enabled</i>081310178179</h5>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/ikhwan.png" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">Mr. Fathan Rizqi</h4>
              <h5 class="card-title"><i class="material-icons">phone_enabled</i>087877247366</h5>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/ikhwan.png" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">Mr. Purwanto</h4>
              <h5 class="card-title"><i class="material-icons">phone_enabled</i>085890977868</h5>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/muslimah.png" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">Mrs. Siska Lestari Apriani</h4>
              <h5 class="card-title"><i class="material-icons">phone_enabled</i>0818808480</h5>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/muslimah.png" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">Mrs. Alfiani Syafitri</h4>
              <h5 class="card-title"><i class="material-icons">phone_enabled</i>082226009403</h5>
            </div>
          </div>
        </div>
      </div>


    </div>

  </div>
</div>

@endsection
