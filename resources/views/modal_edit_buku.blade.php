<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ubah Data Buku</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{-- <form class="form-horizontal" method="POST" action="{{ route('add_buku') }}"> --}}
            {{ Form::open(array('url' => '/edit_buku','files'=>'true')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Judul Buku</label>
                          {{ Form::text('judul_edit', '', array('class' => 'form-control pl-2', 'id' => 'judul_edit')) }}
  
                          @if ($errors->has('judul_edit'))
                            <span class="help-block text-danger">
                                <small>Judul Buku belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kelas</label>
                          {{ Form::select('id_level_edit', $levels, 0, array('class' => 'form-control pl-2', 'id' => 'id_level_edit', 'onchange' => 'get_sub_level_edit()')) }}
  
                          @if ($errors->has('id_level_edit'))
                            <span class="help-block text-danger">
                                <small>Kelas belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Sub Kelas</label>
                          {{ Form::select('id_sub_level_edit', array('' => 'Pilih Sub Level'), 0, array('class' => 'form-control pl-2', 'id' => 'id_sub_level_edit')) }}
  
                          @if ($errors->has('id_sub_level_edit'))
                            <span class="help-block text-danger">
                                <small>Sub Kelas belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row" style="display: none;">
                    <div class="col-md-12">
                      <div>
                        <label class="bmd-label-floating">Upload File</label>
                        <div>
                        {{ Form::file('file_upload_edit') }}
  
                        <br>
                        @if ($errors->has('file_upload_edit'))
                          <span class="help-block text-danger">
                              <small>{{ $errors->first('file_upload_edit') }}</small>
                          </span>
                        @endif
                        </div>
                      </div>
                    </div>
                </div>
  
                    <input type="hidden" name="id_user_edit" id="id_user_edit" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="id_buku" id="id_buku">
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanEdit" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanEdit" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanEdit').click(function() {

  if(confirm('Data sudah benar?') ){
      $('#btnSimpanEdit').hide()
      $('#btnLoadSimpanEdit').show()
      return true;
  } else {
      return false;
  }
});

function get_sub_level_edit(id_sub_level) {
    var id_level = $('#id_level_edit').val();

    $.ajax({
        url: '{{ route("get_sub_levels_buku") }}',
        data: 'id_level=' + id_level,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level_edit').empty();
            $('#id_sub_level_edit').append('<option value="">Pilih Rombel</option>');
            $.each(data, function(key, value) {
                var selected = '';
                if(key == id_sub_level) {
                  selected = 'selected';
                }

                $('#id_sub_level_edit').append('<option value="'+ key +'" ' + selected + '>'+ value +'</option>');
            });
        }
    });
}

function set_edit(id) {
  $.ajax({
      url: '{{ route("get_buku_edit") }}',
      data: 'id=' + id,
      type: "GET",
      dataType: "json",
      success: function(data) {
        $('#id_buku').val(data.id);
        $('#judul_edit').val(data.judul);
        $('#judul_edit').trigger("change");
        $('#id_level_edit').val(data.id_level);
        get_sub_level_edit(data.id_sub_level);
      }
  });
  
}
  
</script>