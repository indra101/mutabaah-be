@extends('layouts.app')

@section('content')

<style>

  .select2-container--material {
      width: 100% !important;
      
      ::placeholder {
          color: inherit;
  }
      
</style>

<div class="content">
  <div class="container-fluid">

    <span style="font-size: smaller;"><a href='{{route('riwayat_baca')}}'>Riwayat Baca</a> <i class="fa fa-chevron-right ml-2 mr-2" style="font-size: smaller;"></i> Tambah Baca Buku</span>

    <div class="main-card mb-3 card">
      
      <div class="card-header card-header-warning">
        <h3 class="card-title">Tambah Baca Buku - {{$siswa->nama}}</h3>
      </div>

      <div class="card-body mt-4">

        @php 
          if(empty($riwayat)) {
            $route = 'add_riwayat';
          } else {
            $route = 'edit_riwayat';
          }
        @endphp

        {{ Form::open(array('url' => '/'.$route,'files'=>'true')) }}
        @csrf

          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label class="bmd-label-floating">Tanggal</label>
                <input type="text" name="tgl_baca" class="form-control datepicker" value="{{ (empty($riwayat)) ? '' : $riwayat->tgl_baca }}" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="bmd-label-floating">Book Title</label>
                <input type="text" name="judul" class="form-control" value="{{ (empty($riwayat)) ? '' : $riwayat->judul }}" required>
                {{-- {{ Form::select('id_buku', $bukus, (empty($riwayat->id_buku)) ? 0 : $riwayat->id_buku, array('class' => 'form-control pl-2 js-select2', 'id' => 'id_buku', 'required' => 'required')) }} --}}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="bmd-label-floating">Written By</label>
                <input type="text" name="written_by" class="form-control" value="{{ (empty($riwayat)) ? '' : $riwayat->written_by }}" required>
              </div>
            </div>
          </div>
          <div class="row mt-4">
            <div class="col-md-6">
              <div>
                <label class="bmd-label-floating">Upload Foto Summary</label>
                <div>
                
                  <div>
                    <img id="image_preview_container" class="img-fluid" src="{{ (empty($riwayat)) ? '' : asset('images')."/laporan/$riwayat->id_siswa/$riwayat->file" }}"  style="margin: auto; max-height: 400px; border-radius: 5px;">
                  </div>
                  <div class="mt-4">
                      <a href="#" class="btn btn-success" id="btn_upload"><i class="material-icons mr-2">insert_photo</i>Upload File</a>
                      <input type="file" name="file_upload" placeholder="Choose image" id="image" style="display: none;" {{ (empty($riwayat)) ? 'required' : '' }}>
                  </div>
                  <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                  <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div>


                </div>
              </div>
            </div>
            
          </div>

          <input type="hidden" name="id_siswa" value="{{$siswa->id}}"/>
          <input type="hidden" name="id_riwayat" value="{{ (empty($riwayat)) ? '' : $riwayat->id }}"/>
          
          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          <div class="clearfix"></div>
        </form>
      </div>

    </div>
  </div>

</div>

<script>

$('#btn_upload').click(function() {
  $('#image').click();
});

$('#image_preview_container').click(function() {
  $('#image').click();
});

$('#image').change(function(e){

var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
var name = this.files[0].name;
var name_arr = name.split(".");
var type = name_arr[name_arr.length - 1];
var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
var pesan_size = "\nMohon masukkan file dengan ukuran max. 1 MB";
var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
var cek_size = size >= 10; // max file size 10 MB
var cek_tipe = allowed.indexOf(type) == -1;

if(cek_size || cek_tipe) {
    var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
    if(cek_size)
        pesan += pesan_size;

    if(cek_tipe)
        pesan += pesan_tipe;

    alert(pesan);
    this.value = '';

} else {
    
    let reader = new FileReader();
    reader.onload = (e) => { 
        $('#image_preview_container').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 

}

});

$(document).ready(function domReady() {
    $("#id_buku").select2({
        placeholder: "Pilih Judul Buku",
        theme: "material",
    });
    
    // $(".select2-selection__arrow")
    //     .addClass("material-icons")
    //     .html("arrow_drop_down");
});

</script>
@stack('scripts')

    

@endsection
