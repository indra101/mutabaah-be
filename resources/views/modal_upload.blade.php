<div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Files</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/uploadFolder')) }}
            @csrf

                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama Folder</label>
                          {{ Form::text('folder', '', array('class' => 'form-control pl-2', 'required')) }}
  
                          @if ($errors->has('folder'))
                            <span class="help-block text-danger">
                                <small>Nama Folder belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kelas</label>
                          {{ Form::select('id_level_upload', $levels, 0, array('class' => 'form-control pl-2', 'id' => 'id_level_upload', 'onchange' => 'get_sub_level_upload()', 'required')) }}
  
                          @if ($errors->has('id_level_upload'))
                            <span class="help-block text-danger">
                                <small>Kelas belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Rombel</label>
                          {{ Form::select('id_sub_level_upload', array('' => 'Pilih Rombel'), 0, array('class' => 'form-control pl-2', 'id' => 'id_sub_level_upload', 'required')) }}
  
                          @if ($errors->has('id_sub_level_upload'))
                            <span class="help-block text-danger">
                                <small>Rombel belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanUpload" class="btn btn-primary" type="submit" value="Upload"/>
                    <button id="btnLoadSimpanUpload" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanUpload').click(function() {

  if(confirm('Data sudah benar?') ){
      $('#btnSimpanUpload').hide()
      $('#btnLoadSimpanUpload').show()
      return true;
  } else {
      return false;
  }
});

function get_sub_level_upload() {
    var id_level = $('#id_level_upload_buku').val();

    $.ajax({
        url: '{{ route("get_sub_levels") }}',
        data: 'id_level=' + id_level,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level_upload').empty();
            $('#id_sub_level_upload').append('<option value="">Pilih Rombel</option>');
            $.each(data, function(key, value) {
                $('#id_sub_level_upload').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

</script>