<div id="app">

    <div class="wrapper ">

      @include('notif')

      @auth
        @include('layouts.sidebar')
      @endauth

        <div class="main-panel">

          @auth
            @include('layouts.header')
          @endauth


            @yield('content')

            @auth
            @include('layouts.footer')
            @endauth

        </div>

    </div>
</div>