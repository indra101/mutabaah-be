<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material-dashboard') }}/assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
            <img src="{{ asset('images') }}/rb-text.png" class="img-fluid mx-auto d-block"/>
            <img src="{{ asset('images') }}/logo.png" style="height: 50px; "/>
        </a>
    </div>

    @php 
      $role = Auth::user()->id_role;
      if(empty($menu)) $menu = 'home';
    @endphp

    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item @if($menu == 'home') active @endif">
          <a class="nav-link" href="{{route('home')}}">
            <i class="material-icons">dashboard</i>
            <p>Beranda</p>
          </a>
        </li>

        {{-- @if($role == 1 || $role == 2) --}}

        
        <li class="nav-item @if($menu == 'ibadah') active @endif">
          <a class="nav-link" href="{{route('ibadah')}}">
            <i class="material-icons">mosque</i>
            <p>Daftar Ibadah</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'aktivitas') active @endif">
          <a class="nav-link" href="{{route('aktivitas')}}">
            <i class="material-icons">assignment</i>
            <p>Daftar Aktivitas Pekanan</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'users') active @endif">
          <a class="nav-link" href="{{route('users')}}">
            <i class="material-icons">supervisor_account</i>
            <p>Daftar User</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'unit') active @endif">
          <a class="nav-link" href="{{route('unit')}}">
            <i class="material-icons">hub</i>
            <p>Daftar Unit</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'pekan') active @endif">
          <a class="nav-link" href="{{route('pekan')}}">
            <i class="material-icons">event</i>
            <p>Daftar Pekan</p>
          </a>
        </li>
        
        {{-- @endif --}}

      </ul>
    </div>
  </div>