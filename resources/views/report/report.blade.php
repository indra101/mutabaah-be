@extends('layouts.app')

@section('content')

<script src="{{ asset('js') }}/html2canvas.min.js"></script>

<style>

.select2-container--material {
    width: 100% !important;
    
    ::placeholder {
        color: inherit;
}
    
</style>

<div class="content">
    <div class="container-fluid">

        <div class="main-card mb-3 card">
        
            <div class="card-header card-header-success">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <ul class="nav nav-tabs" data-tabs="tabs">

                        @php $i = 0; @endphp
                        @foreach ($levels as $level)
                        
                        <li class="nav-item">
                          <a class="nav-link @if($level->id == $curr_level->id) active @else @endif" href="{{route('report').'?id_level='.$level->id}}">
                            <i class="material-icons">face</i> {{$level->nama}}
                            <div class="ripple-container"></div>
                          </a>
                        </li>
        
                        @endforeach
                        
                      </ul>
                    </div>
                  </div>
            </div>

            <div class="card-body">

                <h3>Kelas <span id='kelas'>{{$curr_level->nama}}</span></h3>
                {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
                <br/><br/>
                <div class="overflow-auto">
                    <table class="table table-striped table-hover" id="report-table" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="max-width: 100px;"><strong>No</strong></th>
                                <th ><strong>Nama</strong></th>
                                <th ><strong>Kelas</strong></th>
                                <th ><strong>Jumlah Buku Dibaca</strong></th>
                                <th ><strong></strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>

        <div class="main-card mb-3 card">
            <div class="card-body">


                @foreach($curr_level->sub_levels as $key => $sub)

                    <div id='grafik{{$key}}'>
                        <h3>{{$sub->nama}}</h3>
                        <div id='chart{{$key}}' class="ct-chart"></div>
                    </div>
                    <div class='pt-4'>
                        <a id='dlBtn{{$key}}' href="#">Download Grafik</a>
                    </div>

                @endforeach

            </div>
        </div>

    </div>
</div>

<script>

var table;

$(function() {

    table = $('#report-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('get_reports') !!}?id_level={{$curr_level->id}}",
            data: {
                //"id_level": "{{$levels[0]->id}}"
            },
        },
        columns: [
            { data: 'no', name: 'no' , searchable: true},
            { data: 'nama', name: 'nama' , searchable: true},
            { data: 'nama_rombel', name: 'nama_rombel' , searchable: true},
            { data: 'jml_baca', name: 'jml_baca' , searchable: true},
            { data: null, name: 'action', render: function ( data, type, row ) {
                return '';
            } },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "searchable": false,
            "orderable": false,
            "targets": 0
            }]
    });

    // Grafik Baca Buku

    var options = {
        low: 0,
        high: 15,
        seriesBarDistance: 10,
        height : 500,
        axisY: {
            onlyInteger: true,
            offset: 20
        }
  
    };

    var responsiveOptions = [
        ['screen and (max-width: 640px)', {
            low: 0,
            high: 15,
            seriesBarDistance: 5,
            axisX: {
                labelInterpolationFnc: function (value) {
                    return value[0];
                }
            }
        }]
    ];

    @php $n = 0; @endphp

    @foreach($siswas as $ssw)

        var data = {
            labels: JSON.parse('{{$ssw}}'.replace(/&quot;/g, '"').replace(/&#039;/g, "'")),
            series: [
                JSON.parse('{{$jmls[$n]}}'.replace(/&quot;/g, '"')),
            
            ]
        };

        var chart{{$n}} = new Chartist.Bar('#chart{{$n}}', data, options, responsiveOptions);

        md.startAnimationForBarChart(chart{{$n}});

        @php $n++; @endphp

    @endforeach

    // End Grafik Baca Buku
    
});

function ubah_level(id, kelas) {
    $('#kelas').html(kelas);
    table.ajax.url("{!! route('get_reports') !!}" + "?id_level=" + id);
    table.ajax.reload();
}

function get_data(id) {
    var id_level = $('#id_level').val();

    $.ajax({
        url: "{!! route('get_reports') !!}" + "?id_level=" + id,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level').empty();
            $('#id_sub_level').append('<option value="">Pilih Sub Level</option>');
            $.each(data, function(key, value) {
                $('#id_sub_level').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

$('#dlBtn0').click(function(){
    $(window).scrollTop(0);
    //alert('asda')
    //alert(document.URL)
    //get the div content
    div_content = document.getElementById("chart0")
    //make it as html5 canvas
    html2canvas(div_content).then(function(canvas) {
        //change the canvas to jpeg image
        var data = canvas.toDataURL('image/jpeg', 1.0);
        
        //then call a super hero php to save the image
        save_img(data);
    });
}); 

function save_img(uri){
    var link = document.createElement('a');
    var filename = 'canvas.png';
    if (typeof link.download === 'string') {
      link.href = uri;
      link.download = filename;

      //Firefox requires the link to be in the body
      document.body.appendChild(link);

      //simulate click
      link.click();

      //remove the link when done
      document.body.removeChild(link);
    } else {
      window.open(uri);
    }
}


</script>

<style>

foreignObject {
    font-size: smaller;
}

foreignObject .ct-horizontal {

    color: black !important;

    justify-content: center !important;

    line-height: 10px;
    height: 10px;

    /* Safari */
    -webkit-transform: rotate(-25deg);

    /* Firefox */
    -moz-transform: rotate(-90deg);

    /* IE */
    -ms-transform: rotate(-90deg);

    /* Opera */
    -o-transform: rotate(-90deg);

    /* Internet Explorer */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}

foreignObject .ct-vertical {

    color: black !important;
    line-height: 5px;
    height: 5px;

}

</style>

@stack('scripts')

@endsection
