@extends('layouts.app')

@section('content')

<style>

.page-thumb:hover {
  height: 105px !important;
}

.page-thumb {
  margin-right: 7px; 
  margin-top: 5px; 
  height: 75px;
  transition-duration: .2s;
  display: inline-block;
}

/* The heart of the matter */
.testimonial-group > .row {
  overflow-x: auto;
  white-space: nowrap;
}
.testimonial-group > .row > .col-xs-4 {
  display: inline-block;
  float: none;
}

.thumb-grup {
  overflow: auto;
  white-space: nowrap;
  height: 120px;
}

.carousel-control-next-icon, .carousel-control-prev-icon {
  background-color: grey;
  border-radius: 10px;
  background-size: 70% 100%;
}

</style>

<div class="content">
  <div class="container-fluid">

    @if(Auth::user()->id_role == 3 || $from != 'list') 
      <span style="font-size: smaller;"><a href='/katalog_level/{{ $buku->sub_level->id_level }}'>Daftar Buku</a> <i class="fa fa-chevron-right ml-2 mr-2" style="font-size: smaller;"></i> {{ $buku->judul }}</span>
    @else
      <span style="font-size: smaller;"><a href='{{route('list_book')}}'>Pengaturan Buku</a> <i class="fa fa-chevron-right ml-2 mr-2" style="font-size: smaller;"></i> {{ $buku->judul }}</span>
    @endif

<div class="card" style="">
  <div class="card-header card-header-primary">
    <h3 class="card-title">{{ $buku->judul }}</h3>
    <p class="card-category">Written By: Reading A-Z
    </p>
  </div>
  <div class="card-body">
    <div class="row justify-content-center">

<div id="item_fotos" class="carousel slide shadow" data-interval="false">
  <ol class="carousel-indicators">
      @php
      $n = 0
      @endphp
      @for($i = 0; $i < $buku->jml_hal; $i++)
          
              @if($i == 0)
              <li id="slide{{ $i }}" data-target="#item_fotos" data-slide-to="{{ $i }}" class="active"></li>
              @else
              <li id="slide{{ $i }}" data-target="#item_fotos" data-slide-to="{{ $i }}"></li>
              @endif
          
      @endfor

  </ol>
  <div class="carousel-inner">
      @php
      $i = 0
      @endphp
      @for($i = 0; $i < $buku->jml_hal; $i++)
          
              @if($i == 0)
              <div class="carousel-item active">
              @else
              <div class="carousel-item">
              @endif
                  <div style="max-height: 900px; ">
                      <img class="img-fluid d-block" src="{{ asset('buku/'.$buku->id) }}/page-{{ $i+1 }}.jpg" alt="slide" style="margin: auto; max-height: 700px; ">
                  </div>
              </div>
          
      @endfor

  </div>
  <a class="carousel-control-prev" href="#item_fotos" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#item_fotos" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
  </a>
</div>

</div>

<div class="row justify-content-center">

<div class="mt-4 mb-4" style="width: 100%; text-align: center;">
  @php
  $j = 0
  @endphp

  <div class="align-middle thumb-grup">
    @for($i = 0; $i < $buku->jml_hal; $i++)
    <a href="#" data-target="#item_fotos" data-slide-to="{{ $i }}"> <img src="{{ asset('buku/'.$buku->id) }}/thumb/page-{{ $i+1 }}.jpg" class="page-thumb shadow" alt="slide"></a>
    @endfor
  </div>
</div>


</div>
</div>
</div>

</div>
</div>

<script>

$("body").on("contextmenu", "img", function(e) {
  return false;
});

$(".carousel").on("touchstart", function(event){
        var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function(event){
        var xMove = event.originalEvent.touches[0].pageX;
        if( Math.floor(xClick - xMove) > 5 ){
            $(this).carousel('next');
        }
        else if( Math.floor(xClick - xMove) < -5 ){
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function(){
            $(this).off("touchmove");
    });
});

</script>

@endsection
