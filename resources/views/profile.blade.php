@extends('layouts.app')

@section('content')

<style>

.card-profile .card-avatar {
    max-width: 300px;
    max-height: 300px;
}

</style>

<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-8">

        <div class="main-card mb-3 card">
          
          <div class="card-header card-header-info">
            <h4 class="card-title">Ubah Profil</h4>
            <p class="card-category">Lengkapi Profil Anda</p>
          </div>

          <div class="card-body">

            {{ Form::open(array('url' => '/edit_profil','files'=>'true')) }}
            @csrf
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nama</label>
                    <input type="text" name='nama' value='{{$user->name}}' class="form-control @error('nama') is-invalid @enderror" autocomplete="nama" autofocus required>

                    @error('nama')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Alamat Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" disabled>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nomor Handphone</label>
                    <input type="number" name="hp" value='{{$user->hp}}' class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Password (Kosongkan bila tidak berubah)</label>
                    <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" autocomplete="password" autofocus>
                    {{-- <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus> --}}
                    <span id="msg_pass" style="display: none; color: red; font-size: small;">Password tidak sama!</span>

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Ulangi Password</label>
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="form-group">
                      <label class="bmd-label-floating">Alamat</label>
                      <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" autocomplete="alamat" autofocus rows="5" required>{{$user->alamat}}</textarea>
                    </div>
                  </div>
                </div>
              </div>

              <button class="btn btn-primary pull-right" id="btnSimpan">Simpan Profil</button>
              <div class="clearfix"></div>
            
          </div>

        </div>

      </div>

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images/profil/'. $user->foto) }}" />
            </a>
          </div>
          <div class="card-body">
            {{-- <img class="img-fluid rounded-circle shadow" id="image_preview_container" src="{{ asset('images/profil/'. $user->foto) }}" style="border-radius: 10px;"/> --}}
            <div class="mb-2">
              <h6 class="card-category text-gray">{{$user->nama_role}}</h6>
              <h4 class="card-title">{{$user->name}}</h4>
            </div>
            
            <a href="#" id="btn_upload" class="btn btn-primary btn-round">Ubah Foto</a>
            <input type="file" name="file_upload" placeholder="Choose image" id="image" style="display: none;">
          </div>
        </div>
      </div>

      <button type="submit" id="btnSubmit" class="" style="display: none;"></button>

    {{Form::close()}}

    </div>

  </div>
</div>

<script>
    
$('#btnSimpan').click(function() {
  // var konf = konfirmasi();

  // alert(konf);

  // if(konf) {
  //   $('#btnSubmit').click();
  // } else {
    
  // }

  $('#btnSubmit').click();
  
});

$('#btn_upload').click(function() {
  $('#image').click();
});

$('#image_preview_container').click(function() {
  $('#image').click();
});

function konfirmasi() {
  if(confirm('Data sudah benar?')){

    var pass = $('#password').val();

    var konfirmasi = $('#re-password').val();

    if(pass != '') {
        if(pass !== konfirmasi) {
            $('#msg_pass').show();
            return false;
        } else {
            $('#msg_pass').hide();
        }
    }

    return true;    

  } else {
      return false;
  }
}

$('#image').change(function(e){

  var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
  var name = this.files[0].name;
  var name_arr = name.split(".");
  var type = name_arr[name_arr.length - 1];
  var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
  var pesan_size = "\nMohon masukkan file dengan ukuran max. 1 MB";
  var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
  var cek_size = size >= 10; // max file size 10 MB
  var cek_tipe = allowed.indexOf(type) == -1;

  if(cek_size || cek_tipe) {
      var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
      if(cek_size)
          pesan += pesan_size;

      if(cek_tipe)
          pesan += pesan_tipe;

      alert(pesan);
      this.value = '';

  } else {
      
      let reader = new FileReader();
      reader.onload = (e) => { 
          $('#image_preview_container').attr('src', e.target.result); 
      }
      reader.readAsDataURL(this.files[0]); 

  }

});

</script>
@stack('scripts')

@endsection
