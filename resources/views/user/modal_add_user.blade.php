<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah User</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/add_user')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama User</label>
                          {{ Form::text('name', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}
  
                          @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <small>Nama User belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Username</label>
                        {{ Form::text('username', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}

                        @if ($errors->has('username'))
                          <span class="help-block text-danger">
                              <small>Username belum diisi</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Email</label>
                        {{ Form::text('email', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}

                        @if ($errors->has('email'))
                          <span class="help-block text-danger">
                              <small>Email belum diisi</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Unit</label>
                        {{ Form::select('unit_id', $units, 0, array('class' => 'form-control pl-2 js-select2', 'id' => 'unit_id', 'required' => 'required')) }}

                        @if ($errors->has('unit_id'))
                          <span class="help-block text-danger">
                              <small>Unit belum dipilih</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Peran</label>
                        {{ Form::select('role_id', $roles, 0, array('class' => 'form-control pl-2 js-select2', 'id' => 'role_id', 'required' => 'required')) }}

                        @if ($errors->has('role_id'))
                          <span class="help-block text-danger">
                              <small>Peran belum dipilih</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
    $('#btnSimpanAdd').hide()
    $('#btnLoadSimpanAdd').show()
    return true;
  } else {
    return false;
  }

});

$(document).ready(function domReady() {
    $("#unit_id").select2({
        placeholder: "Pilih Unit",
        theme: "material",
        dropdownParent: $("#modal_add")
    });

    $("#role_id").select2({
        placeholder: "Pilih Peran",
        theme: "material",
        dropdownParent: $("#modal_add")
    });
});

</script>