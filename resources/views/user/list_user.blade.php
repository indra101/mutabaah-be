@extends('layouts.app')

@section('content')

<style>

.select2-container--material {
    width: 100% !important;
    
    ::placeholder {
        color: inherit;
}
    
</style>

<div class="content">
    <div class="container-fluid">

        <div class="main-card mb-3 card">
        
            <div class="card-header card-header-warning">
                <h3 class="card-title">Daftar User</h3>
            </div>

            <div class="card-body">

                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah User</a>
                {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
                <br/><br/>
                <div class="overflow-auto">
                    <table class="table table-striped table-hover" id="user-table" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="max-width: 100px;"><strong>No</strong></th>
                                <th ><strong>Nama</strong></th>
                                <th ><strong>Username</strong></th>
                                <th ><strong>Email</strong></th>
                                <th ><strong>Role</strong></th>
                                <th ><strong>Unit</strong></th>
                                <th ><strong></strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@include('user.modal_add_user')
@include('user.modal_edit_user')

<script>
$(function() {

    var table = $('#user-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('get-users1') !!}",
            data: {
                
            },
        },
        columns: [
            { data: 'no', name: 'no' , searchable: true},
            { data: 'name', name: 'name' , searchable: true},
            { data: 'username', name: 'username' , searchable: true},
            { data: 'email', name: 'email' , searchable: true},
            { data: 'role', name: 'role' , searchable: true},
            { data: 'unit', name: 'unit' , searchable: true},
            { data: null, name: 'action', render: function ( data, type, row ) {
                return '<a href="#reset-' + data.id + '" class="btn btn-warning" style="margin-left: 5px; padding: 10px;" onclick="confirmReset(' + data.id + ', \'' + data.name + '\')" title="Reset Password">Reset Password</a>' +
                '<a href="#" title="Edit Data User" data-toggle="modal" data-target="#modal_edit" style="padding: 10px;" onclick="set_edit(' + data.id + ')"><i class="material-icons">create</i></a>' + 
                '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick="confirmDel(' + data.id + ', \'' + data.name + '\')" title="Hapus"><i class="material-icons">delete</i></a>' +
                '<form class="delete" action="{{route('delete_ortu')}}" method="post">@csrf' + 
                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                '</form>' +
                '<form action="{{route('reset_pass')}}" id="resetForm" method="post">@csrf' + 
                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                '</form>' +
                '</div>';
            } },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "searchable": false,
            "orderable": false,
            "targets": 0
            }]
    });
    
});

function confirmDel(id, name) {
    var txt;
    var r = confirm("Yakin akan menghapus data User? \n\nNama User: " + name);
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#delButton'+id).click();
    } else {
        txt = "You pressed Cancel!";
    }
}

function confirmReset(id, name) {
    var txt;
    var r = confirm("Yakin akan reset password User? \n\nNama User: " + name + "\nPassword baru: 12345678");
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#resetForm').submit();
    } else {
        txt = "You pressed Cancel!";
    }
}

</script>
@stack('scripts')

@endsection
