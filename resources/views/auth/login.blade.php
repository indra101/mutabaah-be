@extends('layouts.app')

@section('content')

<style>

.wrapper {
    background-image: url("{{ asset('images') }}/bg_rb1.png");
    background-size: cover;
    background-repeat: repeat;
}

.Absolute-Center {
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
}

.Absolute-Center.is-Responsive {
  width: 50%; 
  height: 50%;
  min-width: 200px;
  max-width: 400px;
  padding: 40px;
}

.main-panel {
    float: none;
    width: 100%;
    top: 20%;
}

.input-field {
    padding-left: 5px;
}

.rb-image {
    max-width: 100%;
    height: 100%;
}

@media only screen and (max-width: 700px){
    .main-panel {
        float: none;
        width: 100%;
        top: 0;
    }

    .Absolute-Center.is-Responsive {    
        width: 100%;
    }

    .input-field {
        padding-left: 30px;
    }

    .rb-div {
        height: 180px;
    }

}

</style>

<div class="container">
    <div class="row">
        <div class="rb-div">
        <img src="{{ asset('images') }}/rb-text.png" class="rb-image"/> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="card card-login shadow">
            <div class="card-header card-header-warning text-center">
                
                <div class="social-line">
                    <img src="{{ asset('images') }}/logo.png" class="img-fluid"/> 
                </div>
                <h4 class="card-title"></h4>
            </div>
            <div class="card-body ">
                <p class="card-description text-center"><strong>Masukkan Username dan Password</strong></p>
                
                </span>
                <span class="bmd-form-group">
                <div class="input-group">
                    <label for="email" class="col-md-12 col-form-label">{{ __('Username') }}</label>

                    <div class="col-md-1">
                        <i class="material-icons pt-2" style="position: absolute;">person</i>
                    </div>
                    <div class="col-md-10">
                        <input id="email" type="text" class="form-control input-field" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="help-block" style="font-size: smaller; color: red;">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                </span>
                <span class="bmd-form-group">
                <div class="input-group mt-2">
                    <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                    <div class="col-md-1">
                        <i class="material-icons pt-2" style="position: absolute;">lock_outline</i>
                    </div>
                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control input-field" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="help-block" style="font-size: smaller; color: red;">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                </div>
                </span>
            </div>
            <div class="card-footer justify-content-center">
                <button type="submit" class="btn btn-warning">
                    {{ __('Login') }}
                </button>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
