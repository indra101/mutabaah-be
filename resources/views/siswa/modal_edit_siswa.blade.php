<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ubah Data Siswa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/edit_siswa')) }}
            @csrf
        
            <div class="modal-body">
  
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Nama Siswa</label>
                      {{ Form::text('nama_edit', '', array('class' => 'form-control pl-2', 'id' => 'nama_edit', 'required' => 'required')) }}

                      @if ($errors->has('nama_edit'))
                        <span class="help-block text-danger">
                            <small>Nama siswa belum diisi</small>
                        </span>
                      @endif
                    </div>
                  </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nama Panggilan</label>
                    {{ Form::text('panggilan', '', array('class' => 'form-control pl-2', 'id' => 'panggilan', 'required' => 'required')) }}

                    @if ($errors->has('panggilan'))
                      <span class="help-block text-danger">
                          <small>Nama panggilan belum diisi</small>
                      </span>
                    @endif
                  </div>
                </div>
            </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-$request->id_siswagroup">
                    <label class="bmd-label-floating">Kelas</label>
                    {{ Form::select('id_level_edit', $levels, 0, array('class' => 'form-control pl-2', 'id' => 'id_level_edit', 'onchange' => 'get_sub_level_edit()', 'required' => 'required')) }}

                    @if ($errors->has('id_level_edit'))
                      <span class="help-block text-danger">
                          <small>Kelas belum dipilih</small>
                      </span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Rombel</label>
                    {{ Form::select('id_sub_level_edit', array('' => 'Pilih Rombel'), 0, array('class' => 'form-control pl-2', 'id' => 'id_sub_level_edit', 'required' => 'required')) }}

                    @if ($errors->has('id_sub_level_edit'))
                      <span class="help-block text-danger">
                          <small>Rombel belum dipilih</small>
                      </span>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Orang Tua</label>
                    {{ Form::select('id_ortu_edit', $ortus, 0, array('class' => 'form-control pl-2 js-select2', 'id' => 'id_ortu_edit', 'required' => 'required')) }}

                    @if ($errors->has('id_ortu_edit'))
                      <span class="help-block text-danger">
                          <small>Orang Tua belum dipilih</small>
                      </span>
                    @endif
                  </div>
                </div>
              </div>

            </div>

            <input type="hidden" name="id_siswa" id="id_siswa">
                
            <div class="modal-footer">
                <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                <input id="btnSimpanEdit" class="btn btn-primary" type="submit" value="Simpan"/>
                <button id="btnLoadSimpanEdit" class="btn btn-primary" type="button" style="display: none;" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
            </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanEdit').click(function() {

  if(confirm('Data sudah benar?') ){
    //$('#btnSimpanEdit').hide()
    //$('#btnLoadSimpanEdit').show()
    return true;
  } else {
    return false;
  }

});

function get_sub_level_edit(id_sub_level) {
    var id_level = $('#id_level_edit').val();

    $.ajax({
        url: '{{ route("get_sub_levels_buku") }}',
        data: 'id_level=' + id_level,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level_edit').empty();
            $('#id_sub_level_edit').append('<option value="">Pilih Rombel</option>');
            $.each(data, function(key, value) {
                var selected = '';
                if(key == id_sub_level) {
                  selected = 'selected';
                }

                $('#id_sub_level_edit').append('<option value="'+ key +'" ' + selected + '>'+ value +'</option>');
            });
        }
    });
}

function set_edit(id) {
  $.ajax({
      url: '{{ route("get_siswa_edit") }}',
      data: 'id=' + id,
      type: "GET",
      dataType: "json",
      success: function(data) {
        $('#id_siswa').val(data.id);
        $('#nama_edit').val(data.nama);
        $('#nama_edit').trigger("change");
        $('#panggilan').val(data.panggilan);
        $('#panggilan').trigger("change");
        $('#id_level_edit').val(data.id_level);
        get_sub_level_edit(data.id_sub_level);
        $('#id_ortu_edit').val(data.id_ortu);
        $('#id_ortu_edit').trigger("change");
      }
  });
}

</script>