@extends('layouts.app')

@section('content')


<div class="content">
  <div class="container-fluid">

    <div class="main-card mb-3 card">
      
      <div class="card-header card-header-success">
        <h3 class="card-title">Daftar Kelas</h3>
      </div>

        <div class="card-body">

        {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
        <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
        <br/><br/>
        <div class="overflow-auto">
            <table class="table table-striped table-hover" id="levels-table" style="background-color: white;">
                <thead>
                    <tr>
                        <th style="max-width: 10px;"><strong>No</strong></th>
                        <th style="max-width: 50px;"><strong>Kelas</strong></th>
                        <th style="max-width: 50px;"><strong>Rombel</strong></th>
                        <th style="max-width: 50px;"><strong>Status</strong></th>
                        <th style="max-width: 300px;"><strong></strong></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

  </div>
</div>

{{-- @include('modal_add_buku')
@include('modal_edit_buku') --}}

    <script>
    $(function() {

        var table = $('#levels-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{!! route('get_levels') !!}",
                data: {
                    "id_sub_level": ""
                },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: null, name: 'action', render: function ( data, type, row ) {
                    var res = '';
                    data.sub_level.forEach(element => {
                        res += element.nama + '<br>';
                    }); 

                    return res;
                } },
                { data: null, name: 'action', render: function ( data, type, row ) {
                    var status_txt = (data.status == 1) ? 'Aktif' : 'Tidak Aktif';
                    var is_aktif = (data.status == 1) ? 'checked' : '';

                    return '<div class="custom-control custom-switch ml-3" style="display: inline-block">' +
                    '<input type="checkbox" class="custom-control-input" id="customSwitch' + data.no + '" ' + is_aktif + ' onclick="set_status(\'' + data.id + '\')">' + 
                    '<label class="custom-control-label" for="customSwitch' + data.no + '">' + status_txt + '</label>' +
                    '</div>' +
                    '<a id="link' + data.id + '" href="{{route("set_status")}}?id=' + data.id + '" style="display: none;"></a>'
                } },
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<a href="/sub_level/' + data.id + '" title="Atur Rombel" style="padding: 10px;"><i class="material-icons">create</i></a>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus buku? \n\nJudul: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function toasts(status) {
        var pesan = '';
        var tipe = '';

        if(status == true) {
            pesan = 'Berhasil mengaktifkan Kelas';
            tipe = 'success';
        } else {
            pesan = 'Berhasil menon-aktifkan Kelas';
            tipe = 'danger';
        }

        $.notify({
            icon: "add_alert",
            message: pesan
        },{
            type: tipe,
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
    }

    function set_status(id) {
        window.location.href = '{{route("set_status")}}?id=' + id
        // $.ajax({
        //     url: '{{ route("set_status") }}',
        //     data: 'id=' + id,
        //     type: "GET",
        //     dataType: "json",
        //     success: function(data) {
        //         toasts(data.status);
        //     }
        // });
    }

    </script>
    @stack('scripts')

    
</div>
@endsection
