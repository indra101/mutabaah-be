@extends('layouts.app')

@section('content')

<style>

.book-img {
  transition-duration: .1s;
}

.book-img:hover {
  border: solid 4px #FC5185;
}

</style>

<div class="content">
  <div class="container-fluid">
    
    @foreach ($sub_levels as $sub_lvl)

    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon" style="text-align: left;">
        <div class="card-icon">
          <h2><strong>{{ $sub_lvl->level->nama }}</strong> - {{ $sub_lvl->nama }}</h2>
        </div>
        <h3 class="card-title float-left">
          <div class="row ml-2 mr-2 overflow-auto">

           
            @if($sub_lvl->bukus->isEmpty())

            <div class="mr-4" style="height: 120px;">
              <div class="d-flex justify-content-center ">
                <h5>Belum ada buku</h5>
              </div>
            </div>

            @else
              @foreach ($sub_lvl->bukus as $buku)
                  
              <div class="mr-4">
                <div class="d-flex justify-content-center ">
                  <a href="/read/{{ $buku->id }}"> <img src="{{ asset('buku/'.$buku->id) }}/thumb/page-1.jpg" class="book-img rounded shadow" alt="slide" style="max-height: 200px;"></a>
                </div>
                <div class="d-flex justify-content-center mt-2"><h5>{{ $buku->judul }}</h5></div>
              </div>
              
              @endforeach

            @endif

          </div>
        </h3>
      </div>
      {{-- <div class="card-footer">
        <div class="stats">
          <i class="material-icons text-danger">warning</i>
          <a href="javascript:;">Get More Space...</a>
        </div>
      </div> --}}
    </div>

    @endforeach


  </div>
</div>

@endsection
