@extends('layouts.app')

@section('content')

<style>

.select2-container--material {
    width: 100% !important;
    
    ::placeholder {
        color: inherit;
}
    
</style>

<div class="content">
    <div class="container-fluid">

        <div class="main-card mb-3 card">
        
            <div class="card-header card-header-warning">
                <h3 class="card-title">Daftar Aktivitas Pekanan</h3>
            </div>

            <div class="card-body">

                <div class="row mt-2">
                    <div class="col-md-2">
                        <label>Pilih Unit</label>
                        {{ Form::select('id_unit', $units, 0, array('class' => 'form-control pl-2 ml-2', 'id' => 'id_unit', 'onchange' => 'update_table()')) }}
                    </div>
                    <div class="col-md-3">
                        <label>Pilih Pekan</label>
                        <select name='id_pekan' id="id_pekan" class="form-control pl-2 ml-2" onchange="update_table()">
                        @foreach ($pekans as $pk)
                            <option value="{{$pk->id}}">Pekan {{$pk->id}} <label>({{$pk->tgl_start}} s/d {{$pk->tgl_end}})</label></option>
                        @endforeach
                        </select>
                    </div>
                </div>
                {{-- <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah User</a> --}}
                {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
                <br/><br/>
                <div class="overflow-auto">
                    <table class="table table-striped table-hover" id="aktivitas-table" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="max-width: 60px;"><strong>No</strong></th>
                                <th width="150px"><strong>Nama</strong></th>
                                <th width="150px"><strong>Unit</strong></th>
                                <th width="200px"><strong>Pekan</strong></th>
                                <th ><strong>Subuh</strong></th>
                                <th ><strong>Tilawah</strong></th>
                                <th ><strong>Tahajjud</strong></th>
                                <th ><strong>Infaq</strong></th>
                                <th ><strong>Al Ma'tsurat</strong></th>
                                <th ><strong>Olahraga</strong></th>
                                <th ><strong>Pencapaian</strong></th>
                                <th width="120px"><strong></strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@include('ortu.modal_add_ortu')
@include('ortu.modal_edit_ortu')

<script>

var table;

$(function() {

    table = $('#aktivitas-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('get_aktivitass') !!}?id_pekan=" + $('#id_pekan').val() + "&id_unit=" + $('#id_unit').val(),
        },
        columns: [
            { data: 'no', name: 'no' , searchable: true},
            { data: 'user_name', name: 'user_name' , searchable: true},
            { data: 'unit', name: 'unit' , searchable: true},
            { data: null, name: 'target', render: function ( data, type, row ) {
                return data.tgl_start + ' s/d ' + data.tgl_end;
            } },
            { data: 'subuh', name: 'subuh' , searchable: true},
            { data: 'tilawah', name: 'tilawah' , searchable: true},
            { data: 'tahajjud', name: 'tahajjud' , searchable: true},
            { data: 'infaq', name: 'infaq' , searchable: true},
            { data: 'matsurat', name: 'matsurat' , searchable: true},
            { data: 'olahraga', name: 'olahraga' , searchable: true},
            { data: null, name: 'persen', render: function ( data, type, row ) {
                return data.persen + '%';
            } },
            { data: null, name: 'action', render: function ( data, type, row ) {
                return '<a href="#" title="Edit Data OTS" data-toggle="modal" data-target="#modal_edit" style="padding: 10px;" onclick="set_edit(' + data.id + ')"><i class="material-icons">create</i></a>' + 
                '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick="confirmDel(' + data.id + ', \'' + data.nama + '\')" title="Hapus"><i class="material-icons">delete</i></a>' +
                '<form class="delete" action="{{route('delete_ortu')}}" method="post">@csrf' + 
                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +
                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                '</form>' +
                '</div>';
            } },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "searchable": false,
            "orderable": false,
            "targets": 0
        }]
    });

    $('#id_pekan').select2();
    $('#id_unit').select2();
    
});

function update_table() {
    var id_pekan = $('#id_pekan').val();
    var id_unit = $('#id_unit').val();
    table.ajax.url("{!! route('get_aktivitass') !!}?id_pekan=" + id_pekan + "&id_unit=" + id_unit);
    table.ajax.reload();
}

function confirmDel(id, name) {
    var txt;
    var r = confirm("Yakin akan menghapus data OTS? \n\nNama OTS: " + name);
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#delButton'+id).click();
    } else {
        txt = "You pressed Cancel!";
    }
}

function confirmReset(id, name) {
    var txt;
    var r = confirm("Yakin akan reset password OTS? \n\nNama OTS: " + name + "\nPassword baru: 12345678");
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#resetForm').submit();
    } else {
        txt = "You pressed Cancel!";
    }
}

$(document).ready(function domReady() {
    $("#id_ortu").select2({
        placeholder: "Pilih Orang Tua Siswa",
        theme: "material",
        dropdownParent: $("#modal_add")
    });

    $("#id_ortu_edit").select2({
        placeholder: "Pilih Orang Tua Siswa",
        theme: "material",
        dropdownParent: $("#modal_edit")
    });
    
    // $(".select2-selection__arrow")
    //     .addClass("material-icons")
    //     .html("arrow_drop_down");
});

</script>
@stack('scripts')

@endsection
