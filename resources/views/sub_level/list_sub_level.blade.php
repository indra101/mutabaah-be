@extends('layouts.app')

@section('content')


<div class="content">
    <div class="container-fluid">

        <span style="font-size: smaller;"><a href='{{route('level')}}'>Pengaturan Kelas</a> <i class="fa fa-chevron-right ml-2 mr-2" style="font-size: smaller;"></i> Daftar Rombel</span>

        <div class="main-card mb-3 card">
        
            <div class="card-header card-header-warning">
                <h3 class="card-title">Daftar Rombel Kelas {{$level->nama}}</h3>
            </div>

            <div class="card-body">

                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah Rombel</a>
                {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
                <br/><br/>
                <div class="overflow-auto">
                    <table class="table table-striped table-hover" id="sublevels-table" style="background-color: white;">
                        <thead>
                            <tr>
                                <th style="max-width: 10px;"><strong>No</strong></th>
                                <th style="max-width: 50px;"><strong>Rombel</strong></th>
                                <th style="max-width: 300px;"><strong></strong></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

@include('sub_level.modal_add_rombel')
@include('sub_level.modal_edit_rombel')

<script>
$(function() {

    var table = $('#sublevels-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('get_sub_levels') !!}",
            data: {
                "id_level": "{{$level->id}}"
            },
        },
        columns: [
            { data: 'no', name: 'no' , searchable: true},
            { data: 'nama', name: 'nama' , searchable: true},
            { data: null, name: 'action', render: function ( data, type, row ) {
                return '<a href="#" title="Edit Rombel" data-toggle="modal" data-target="#modal_edit" style="padding: 10px;" onclick="set_edit(' + data.id + ', \'' + data.nama + '\')"><i class="material-icons">create</i></a>' + 
                '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick="confirmDel(' + data.id + ', \'' + data.nama + '\')" title="Hapus"><i class="material-icons">delete</i></a>' +
                '<form class="delete" action="{{route('delete_sub_level')}}" method="post">@csrf' + 
                '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                '</form></div>';
            } },
        ],
        columnDefs: [{
            "defaultContent": "-",
            "searchable": false,
            "orderable": false,
            "targets": 0
            }]
    });
    
});

function confirmDel(id, name) {
    var txt;
    var r = confirm("Yakin akan menghapus Rombel? \n\nNama Rombel: " + name);
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#delButton'+id).click();
    } else {
        txt = "You pressed Cancel!";
    }
}

function set_edit(id, nama) {
    $('#id_sub_level').val(id);
    $('#nama_edit').val(nama);
    $('#nama_edit').trigger("change");
}

</script>
@stack('scripts')

@endsection
