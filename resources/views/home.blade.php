@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container-fluid">

        <div class="row">

        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
              <h1>Mutaba'ah Yaumiyah</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <img src="{{ asset('images') }}/islam_bg5.png" class="img-fluid mx-auto d-block" style="height: 500px;"/>
                  </div>
                  <div class="col-md-4">

                    @if(Auth::user()->id_role == 3)

                    @php $n = 1; @endphp

                    @foreach ($siswas as $siswa)

                      @php 
                        if($n == 1) $tipe = 'success'; 
                        else if($n == 2) $tipe = 'warning';
                        else if($n == 3) $tipe = 'info'; 
                        else $tipe = 'primary'; 
                      @endphp

                      <div class="card card-chart shadow">
                        <div class="card-header card-header-{{$tipe}}">
                          <div>
                            <h2><strong>{{$siswa->nama}}</strong></h2>
                          </div>
                          <div><h4>{{$siswa->nama_level}} {{$siswa->nama_sub_level}}</h4></div>
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Sudah Baca</h4>
                          <p class="card-category">
                            <h2>{{$siswa->jml_baca}} Buku</h2>
                          </p>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons">access_time</i> updated 4 minutes ago
                          </div>
                        </div>
                      </div>

                      @php 
                        if($n > 3) $n = 1;
                        else $n++;
                      @endphp
                          
                    @endforeach

                    @endif

                  </div>
                </div>
            </div>
          </div>
        
        
        </div>

    </div>
  </div>
@endsection
