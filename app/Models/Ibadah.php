<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ibadah extends Model
{
    
    protected $fillable = [
        'nama', 'target', 'satuan', 'icon', 'kode'
    ];
    
}
