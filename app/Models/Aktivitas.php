<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aktivitas extends Model
{

    protected $table = 'aktivitass';
    
    protected $fillable = [
        'id_user', 'id_pekan', 'subuh', 'tilawah', 'tahajjud', 'infaq', 'matsurat', 'olahraga'
    ];
    
    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'id_user');
    }

    public function pekan() {
        return $this->hasOne('App\Models\Pekan', 'id', 'id_pekan');
    }
}
