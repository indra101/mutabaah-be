<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'url_auth', 'url_verify', 'url_logout'
    ];
    
}
