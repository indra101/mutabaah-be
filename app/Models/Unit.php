<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    
    protected $fillable = [
        'name', 'kode'
    ];
    
    public function user() {
        return $this->hasMany('App/User');
    }
}
