<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $fillable = [
        'judul', 'id_sub_level', 'jml_hal', 'created_by', 'created_at', 'updated_at', 'updated_by'
    ];

    public function sub_level(){
        return $this->belongsTo('App\Models\SubLevel','id_sub_level');
    }
}
