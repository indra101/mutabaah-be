<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    const ELASTIC_INDEX = 'items';
    const ELASTIC_TYPE  = 'item';
    
    protected $fillable = [
        'nama', 'deskripsi', 'harga', 'stok', 'updated_at'
    ];
}
