<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pekan extends Model
{
    
    protected $fillable = [
        'name', 'tgl_start', 'tgl_end'
    ];

    public function aktivitas() {
        return $this->hasMany('App\Models\Aktivitas');
    }
    
}
