<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'jenis', 'id_user', 'nama_user', 'id_items', 'nama_item', 'harga', 'jumlah_items', 'created_date'
    ];
}
