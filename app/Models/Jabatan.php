<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $fillable = [
        'name'
    ];
    
    public function user() {
        return $this->hasMany('App/User');
    }
}
