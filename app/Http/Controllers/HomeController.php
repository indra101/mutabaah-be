<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Http;
use App\Models\Siswa;
use App\Models\SubLevel;
use App\Models\ActiveLevel;
use App\Models\RiwayatBaca;

use App\Setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $siswas = Siswa::where('id_ortu', Auth::user()->id_ortu)->whereIn('id_sub_level', SubLevel::select('id')->whereIn('id_level', ActiveLevel::select('id_level')->get())->get())->get();

        // foreach($siswas as $siswa) {
        //     $siswa->jml_baca = RiwayatBaca::where('id_siswa', $siswa->id)->count();
        //     $siswa->nama_sub_level = $siswa->sub_level->nama;
        //     $siswa->nama_level = $siswa->sub_level->level->nama;
        // }

        $siswas = [];

        return view('home', ['menu' => 'home', 'siswas' => $siswas]);
    }

    public function login(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $email = $request->email;
        $password = $request->password;
        $url_auth = Setting::first()->url_auth;
        
        $response = $client->request('POST', $url_auth, [
            'form_params' => [
                'email' => $email,
                'password' => $password,
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());

        // echo '<pre>';
        // print_r($response->data->token);
        // print_r($response->data->user[0]->id);

        // die;

        if($response->success) {
            $request->session()->put('username', $email);
            $request->session()->put('email', $email);
            $request->session()->put('role_id', $response->data->user->role_id);
            $request->session()->put('user_id', $response->data->user->id);
            $request->session()->put('name', $response->data->user->name);
            $request->session()->put('address', $response->data->user->address);
            $request->session()->put('hp', $response->data->user->hp);
            $request->session()->put('token', $response->data->token);

            //print_r($request->session()->all());
            //die;

            return redirect()->route('home');
        } else {
            return redirect()->route('login')
                        ->with('error','Login gagal!');
        }
        
        //die;
    }

    public function logout(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $url_logout = Setting::first()->url_logout;
        $token = session('token');
        
        $response = $client->request('POST', $url_logout, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());

        if($response->success) {

            $request->session()->flush();

            return redirect()->route('login')
                        ->with('success','Anda telah logout!');

        } else {
            return redirect()->route('login')
                        ->with('error','Silahkan Login!');
        }
    }

    public function getItemj()
    {
        
        $client = new \GuzzleHttp\Client();

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3BvYy1sa3BwLTIwMjEvcHVibGljL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjEzNDg1MjkwLCJleHAiOjE2MTM0ODg4OTAsIm5iZiI6MTYxMzQ4NTI5MCwianRpIjoiS2tTaURsblBVZFdnandjMyIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.1sglNjcJPOmNv_FLHMbbh2HcfUuIJ01jFH-cNEJ1NVs';
        
        $response = $client->request('GET', 'http://localhost/poc-lkpp-2021/public/api/getAllItem', [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]
        ]);
        $response = $response->getBody()->getContents();
        echo '<pre>';
        print_r($response);
        
        die;
    }
}
