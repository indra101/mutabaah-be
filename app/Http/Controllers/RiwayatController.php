<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;

class RiwayatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function riwayat_baca()
    {
        $bukus = Buku::all();
        $siswas = Siswa::where('id_ortu', Auth::user()->id_ortu)->whereIn('id_sub_level', SubLevel::select('id')->whereIn('id_level', ActiveLevel::select('id_level')->get())->get())->get();
        // print_r(!$siswas->isEmpty());
        // die;
        return view('riwayat_baca', ['menu' => 'riwayat_baca', 'bukus' => $bukus, 'siswas' => $siswas]);
    }

    public function laporan($id_siswa)
    {
        $siswa = Siswa::find($id_siswa);
        $bukus = Buku::pluck('judul', 'id');
        $bukus[0] = 'Pilih Judul Buku';
        return view('laporan', ['menu' => 'riwayat_baca', 'bukus' => $bukus, 'siswa' => $siswa, 'riwayat' => null]);
    }

    public function edit_riwayat_page($id_riwayat)
    {
        $bukus = Buku::all();
        $riwayat = RiwayatBaca::find($id_riwayat);
        $siswa = $riwayat->siswa;
        
        return view('laporan', ['menu' => 'riwayat_baca', 'bukus' => $bukus, 'siswa' => $siswa, 'riwayat' => $riwayat]);
    }

    public function add_riwayat(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tgl_baca' => 'required',
            'written_by' => 'required',
            'file_upload' => 'required|mimes:jpg,JPG,jpeg,JPEG,png,PNG',
        ]);

        $riwayat = new RiwayatBaca;
        $riwayat->id_siswa = $request->id_siswa;
        $riwayat->id_buku = $request->id_buku;
        $riwayat->tgl_baca = $request->tgl_baca;
        $riwayat->judul = $request->judul;
        $riwayat->written_by = $request->written_by;
        $riwayat->save();

        $file_data = array();
        $file_upload = $request->file('file_upload');

        if(!empty($file_upload)) {
            $nama_file = $this->uploadFile($file_upload, $request->id_siswa, $riwayat->id);
        }

        if(!$nama_file) {
            $riwayat->delete();

            return redirect()->route('riwayat_baca')
                        ->with('danger','Gagal menambah bacaan, mohon ulangi lagi!');    
        }

        $riwayat->file = $nama_file;
        $riwayat->save();

        return redirect()->route('riwayat_baca')
                        ->with('success','Berhasil menambah bacaan!');
    }

    public function edit_riwayat(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'tgl_baca' => 'required',
            'written_by' => 'required',
            'file_upload' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG',
        ]);

        $riwayat = RiwayatBaca::find($request->id_riwayat);
        $riwayat->id_buku = $request->id_buku;
        $riwayat->tgl_baca = $request->tgl_baca;
        $riwayat->judul = $request->judul;
        $riwayat->written_by = $request->written_by;
        $file_upload = $request->file('file_upload');

        if(!empty($file_upload)) {
            $nama_file = $this->uploadFile($file_upload, $riwayat->id_siswa, $riwayat->id);

            if(!$nama_file) {
                return redirect()->route('riwayat_baca')
                            ->with('danger','Gagal upload foto, mohon ulangi lagi!');    
            }

            $riwayat->file = $nama_file;
        }
        
        $riwayat->save();

        return redirect()->route('riwayat_baca')
                        ->with('success','Berhasil mengubah bacaan!');
    }

    public function delete_riwayat(Request $request)
    {
        $riwayat = RiwayatBaca::find($request->id);
        $riwayat->delete();

        return redirect()->route('riwayat_baca')
                        ->with('success','Berhasil menghapus bacaan!');
    }

    public function get_riwayats(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'tgl_baca',
                2 =>'judul',
                3 =>'created_at',
        );
        //Getting the data
        $riwayats = RiwayatBaca::join('siswas', 'siswas.id', 'riwayat_baca.id_siswa')
                                ->join('sub_levels', 'sub_levels.id', 'siswas.id_sub_level')
                                ->join('levels', 'levels.id', 'sub_levels.id_level')
                                //->join('bukus', 'bukus.id', '=', 'riwayat_baca.id_buku')
                                ->select('riwayat_baca.*', 'siswas.nama as nama_siswa', 'levels.nama as nama_level', 'sub_levels.nama as nama_sub_level');

        if ($request->has('id_siswa') && $request->input('id_siswa') != '') {
            $riwayats->where('id_siswa', $request->input('id_siswa'));
        }
        
        $totalData = $riwayats->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $riwayats->where ( 'riwayat_baca.judul', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere( 'siswas.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere( 'levels.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere( 'sub_levels.nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $riwayats->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $riwayats->count ();
        // Data to client
        $jobs = $riwayats->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $riwayats = $riwayats->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        // print_r($riwayats);
        // die;

        foreach ( $riwayats as $rw ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $rw->id;
            //$nestedData ['id_buku'] = $rw->id_buku;
            $nestedData ['id_siswa'] = $rw->id_siswa;
            $nestedData ['nama_siswa'] = $rw->nama_siswa;
            $nestedData ['nama_level'] = $rw->nama_level;
            $nestedData ['nama_sub_level'] = $rw->nama_sub_level;
            $nestedData ['judul'] = $rw->judul;
            $nestedData ['written_by'] = $rw->written_by;
            $nestedData ['file'] = $rw->file;
            $nestedData ['created_at'] = date_format($rw->created_at, "d M Y");
            $nestedData ['tot'] = count($riwayats);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    function uploadFile($file, $id_siswa, $id_laporan) {

        $name = $file->getClientOriginalName();
        $path = $file->storeAs('public/files', $name);
        $url = Storage::url($path);

        $destinationPath = public_path()."/images/laporan/$id_siswa/";

        // echo $url;
        // echo '<br>'.$destinationPath;
        // echo '<br>'.public_path().$url;
        // die;

        if(!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true, true);
            //File::makeDirectory($destinationPath.'/thumb/', 0755, true, true);
        }

        try {

            $image_name = "$id_laporan.jpg";

            $img = Image::make(public_path().$url);
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.$image_name);

            Storage::delete($path);

        } catch (Exception $e) {
            return false;    
        }

        return $image_name;
    }
    
}
