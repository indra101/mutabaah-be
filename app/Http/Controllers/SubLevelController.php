<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\sub_levelBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;

class SubLevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id_level)
    {
        $level = Level::find($id_level);
        
        return view('sub_level.list_sub_level', ['menu' => 'level', 'level' => $level]);
    }

    public function add_sub_level(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $id_level = $request->id_level;

        $sub_level = new SubLevel;
        $sub_level->nama = $request->nama;
        $sub_level->id_level = $id_level;
        $sub_level->created_by = Auth::user()->id;
        $sub_level->save();

        return redirect('/sub_level/'.$id_level)
                        ->with('success','Berhasil menambah Rombel!');
    }

    public function edit_sub_level(Request $request)
    {
        $this->validate($request, [
            'nama_edit' => 'required',
        ]);

        $id_level = $request->id_level_edit;

        $sub_level = SubLevel::find($request->id_sub_level);
        $sub_level->nama = $request->nama_edit;
        $sub_level->save();

        return redirect('/sub_level/'.$id_level)
                        ->with('success','Berhasil mengubah Rombel!');
    }

    public function delete(Request $request)
    {
        $sub_level = SubLevel::find($request->id);
        $id_level = $sub_level->id_level;
        $sub_level->delete();

        return redirect('/sub_level/'.$id_level)
                        ->with('success','Berhasil menghapus Rombel!');
    }

    public function get_sub_levels(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
        );
        //Getting the data
        $sub_levels = SubLevel::where('id_level', $request->id_level);

        $totalData = $sub_levels->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $sub_levels->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $sub_levels->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $sub_levels->count ();
        // Data to client
        $jobs = $sub_levels->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $sub_levels = $sub_levels->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $sub_levels as $lvl ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $lvl->id;
            $nestedData ['nama'] = $lvl->nama;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
