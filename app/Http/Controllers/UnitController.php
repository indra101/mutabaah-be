<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Unit;

use Auth;
use DB;

class UnitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('unit.list_unit', ['menu' => 'unit']);
    }

    public function add_unit(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'kode' => 'required',
        ]);

        $unit = new Unit;
        $unit->name = $request->name;
        $unit->kode = $request->kode;
        $unit->save();

        return redirect()->route('unit')
                        ->with('success','Berhasil menambah data unit!');
    }

    public function edit_unit(Request $request)
    {
        $this->validate($request, [
            'name_edit' => 'required',
            'kode_edit' => 'required',
        ]);

        $unit = Unit::find($request->id_unit);
        $unit->name = $request->name_edit;
        $unit->kode = $request->kode_edit;
        $unit->save();

        return redirect()->route('unit')
                        ->with('success','Berhasil mengubah data unit!');
    }

    public function get_unit_edit(Request $request)
    {
        $id = $request->id;
        $unit = Unit::find($id);

        return $unit->toJson();
    }

    public function delete_unit(Request $request)
    {
        Unit::find($request->id)->delete();
        User::where('id_unit', $request->id)->delete();

        return redirect()->route('unit')
                        ->with('success','Berhasil menghapus data unit!');
    }

    public function get_units(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'name',
                2 =>'kode',
        );
        //Getting the data
        $units = DB::table('units');

        $totalData = $units->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $units->where ( 'name', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'kode', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $units->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $units->count ();
        // Data to client
        $jobs = $units->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $units = $units->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $units as $unit ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $unit->id;
            $nestedData ['name'] = $unit->name;
            $nestedData ['kode'] = $unit->kode;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
