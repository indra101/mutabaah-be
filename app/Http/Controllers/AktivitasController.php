<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Aktivitas;
use App\Models\Ibadah;
use App\Models\Pekan;
use App\Models\Unit;

use Auth;
use DB;

class AktivitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pekans = Pekan::orderBy('id', 'DESC')->get();
        $units = Unit::pluck('name', 'id');

        return view('aktivitas.list_aktivitas', ['menu' => 'aktivitas', 'pekans' => $pekans, 'units' => $units]);
    }

    public function add_aktivitas(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            // 'email' => 'unique:App\Models\User,email'
        ]);

        $aktivitas = new Aktivitas;
        $aktivitas->nama = $request->nama;
        $aktivitas->email = $request->email;
        $aktivitas->save();

        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->id_role = 3;
        $user->id_aktivitas = $aktivitas->id;
        $user->password = bcrypt('12345678');
        $user->save();

        return redirect()->route('aktivitas')
                        ->with('success','Berhasil menambah data OTS!');
    }

    public function edit_aktivitas(Request $request)
    {
        $this->validate($request, [
            'nama_edit' => 'required',
            'email_edit' => 'required',
        ]);

        $aktivitas = Aktivitas::find($request->id_aktivitas);
        $aktivitas->nama = $request->nama_edit;
        $aktivitas->email = $request->email_edit;
        $aktivitas->save();

        $user = User::where('id_aktivitas', $request->id_aktivitas)->first();
        $user->name = $request->nama_edit;
        $user->email = $request->email_edit;
        $user->save();

        return redirect()->route('aktivitas')
                        ->with('success','Berhasil mengubah data OTS!');
    }

    public function get_aktivitas_edit(Request $request)
    {
        $id = $request->id;
        $aktivitas = Aktivitas::find($id);

        return $aktivitas->toJson();
    }

    public function delete_aktivitas(Request $request)
    {
        Aktivitas::find($request->id)->delete();
        User::where('id_aktivitas', $request->id)->delete();

        return redirect()->route('aktivitas')
                        ->with('success','Berhasil menghapus data OTS!');
    }

    public function reset_pass(Request $request)
    {
        $user = User::where('id_aktivitas', $request->id)->first();
        $user->password = bcrypt('12345678');
        $user->save();

        return redirect()->route('aktivitas')
                        ->with('success','Berhasil reset password OTS!');
    }

    public function get_aktivitass(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'user_name',
                2 =>'unit',
        );
        //Getting the data
        $aktivitass = DB::table('aktivitass')
                        ->select('aktivitass.*', 'users.name as user_name', 'pekans.tgl_start', 'pekans.tgl_end', 'units.name as unit')
                        ->leftJoin('users', 'users.id', 'aktivitass.id_user')
                        ->leftJoin('pekans', 'pekans.id', 'aktivitass.id_pekan')
                        ->leftJoin('units', 'units.id', 'users.unit_id');

        $totalData = $aktivitass->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                $aktivitass->where ( 'users.name', 'Like', '%' . $searchTerm . '%' )
                        // ->orWhere ( 'email', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        if ($request->has ( 'id_pekan' )) {
            if ($request->input('id_pekan') != '') {    
                $aktivitass->where ( 'id_pekan', $request->id_pekan );
            }
        } else {
            $id_pekan = Pekan::orderBy('id', 'desc')->first()->id;
            $aktivitass->where ( 'id_pekan', $id_pekan );
        }

        if ($request->has ( 'id_unit' )) {
            if ($request->input('id_unit') != '') {    
                $aktivitass->where ( 'units.id', $request->id_unit );
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $aktivitass->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $aktivitass->count ();
        // Data to client
        $jobs = $aktivitass->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $aktivitass = $aktivitass->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $aktivitass as $akt ) {
            $achive = $this->countAchive($akt);

            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id_user'] = $akt->id_user;
            $nestedData ['id_pekan'] = $akt->id_pekan;
            $nestedData ['subuh'] = $akt->subuh;
            $nestedData ['tilawah'] = $akt->tilawah;
            $nestedData ['tahajjud'] = $akt->tahajjud;
            $nestedData ['infaq'] = $akt->infaq;
            $nestedData ['matsurat'] = $akt->matsurat;
            $nestedData ['olahraga'] = $akt->olahraga;
            $nestedData ['user_name'] = $akt->user_name;
            $nestedData ['tgl_start'] = $akt->tgl_start;
            $nestedData ['tgl_end'] = $akt->tgl_end;
            $nestedData ['unit'] = $akt->unit;
            $nestedData ['persen'] = round($achive*100, 0);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    public function countAchive($aktivitas) {
        $ibadah = Ibadah::all();

        $subuh = $aktivitas->subuh / $ibadah[0]->target;
        $tilawah = $aktivitas->tilawah / $ibadah[1]->target;
        $tahajjud = $aktivitas->tahajjud / $ibadah[2]->target;
        $infaq = $aktivitas->infaq / $ibadah[3]->target;
        $matsurat = $aktivitas->matsurat / $ibadah[4]->target;
        $olahraga = $aktivitas->olahraga / $ibadah[5]->target;

        $subuh = ($subuh > 1) ? 1 : $subuh;
        $tilawah = ($tilawah > 1) ? 1 : $tilawah;
        $tahajjud = ($tahajjud > 1) ? 1 : $tahajjud;
        $infaq = ($infaq > 1) ? 1 : $infaq;
        $matsurat = ($matsurat > 1) ? 1 : $matsurat;
        $olahraga = ($olahraga > 1) ? 1 : $olahraga;

        $achive = ($subuh + $tilawah + $tahajjud + $infaq + $matsurat + $olahraga) / 6;

        // echo "achive: $achive";
        // die;

        return $achive;
    }

}
