<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pekan;

use Auth;
use DB;

class PekanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pekan.list_pekan', ['menu' => 'pekan']);
    }

    public function add_pekan(Request $request)
    {
        $this->validate($request, [
            'tgl_start' => 'required',
            'tgl_end' => 'required',
        ]);

        $pekan = new Pekan;
        $pekan->tgl_start = $request->tgl_start;
        $pekan->tgl_end = $request->tgl_end;
        $pekan->save();

        return redirect()->route('pekan')
                        ->with('success','Berhasil menambah data pekan!');
    }

    public function edit_pekan(Request $request)
    {
        $this->validate($request, [
            'tgl_start_edit' => 'required',
            'tgl_end_edit' => 'required',
        ]);

        $pekan = Pekan::find($request->id_pekan);
        $pekan->tgl_start = $request->tgl_start_edit;
        $pekan->tgl_end = $request->tgl_end_edit;
        $pekan->save();

        return redirect()->route('pekan')
                        ->with('success','Berhasil mengubah data pekan!');
    }

    public function get_pekan_edit(Request $request)
    {
        $id = $request->id;
        $pekan = Pekan::find($id);

        return $pekan->toJson();
    }

    public function delete_pekan(Request $request)
    {
        Pekan::find($request->id)->delete();
        User::where('id_pekan', $request->id)->delete();

        return redirect()->route('pekan')
                        ->with('success','Berhasil menghapus data pekan!');
    }

    public function get_pekans(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'tgl_start',
                2 =>'tgl_end',
        );
        //Getting the data
        $pekans = DB::table('pekans');

        $totalData = $pekans->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $pekans->where ( 'name', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'email', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $pekans->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $pekans->count ();
        // Data to client
        $jobs = $pekans->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $pekans = $pekans->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $pekans as $pkn ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $pkn->id;
            $nestedData ['tgl_start'] = $pkn->tgl_start;
            $nestedData ['tgl_end'] = $pkn->tgl_end;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
