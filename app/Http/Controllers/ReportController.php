<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;
use DB;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $levels = Level::whereIn('id', ActiveLevel::select('id_level')->get())->get();

        $id_level = (empty($request->id_level)) ? $levels[0]->id : $request->id_level;

        $curr_level = Level::find($id_level);

        $reports = Siswa::join('sub_levels', 'sub_levels.id', 'siswas.id_sub_level')
                        ->join('levels', 'levels.id', 'sub_levels.id_level')
                        ->select('siswas.*')
                        ->where('sub_levels.id_level', $id_level)->get();
        
        $n = 0;
        foreach ( $reports as $rp ) {
            $rp->jml_baca = $rp->riwayat_bacas->count();
            $reports[$n] = $rp;
            $n++;
        }

        $siswas = array();
        $jmls = array();

        foreach($curr_level->sub_levels as $sub) {
            $siswas[] = $reports->where('id_sub_level', $sub->id)->pluck('panggilan')->toJson();
            $jmls[] = $reports->where('id_sub_level', $sub->id)->pluck('jml_baca')->toJson();
        }


        //$siswas = urlencode($siswas);
        // print_r($curr_level);
        // die;

        return view('report.report', ['menu' => 'report', 'levels' => $levels, 'siswas' => $siswas, 'jmls' => $jmls, 'curr_level' => $curr_level]);
    }

    public function get_reports(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'nama_rombel',
        );

        //Getting the data
        $reports = Siswa::join('sub_levels', 'sub_levels.id', 'siswas.id_sub_level')
                        ->join('levels', 'levels.id', 'sub_levels.id_level')
                        ->select('siswas.*', 'sub_levels.nama as nama_sub_level', 'levels.nama as nama_level')
                        ->where('sub_levels.id_level', $request->id_level)
                        ;
        
        $totalData = $reports->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $reports->where( 'siswas.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere( 'sub_levels.nama', 'Like', '%' . $searchTerm . '%' )
                        ->where('sub_levels.id_level', $request->id_level)
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $reports->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $reports->count ();
        // Data to client
        $jobs = $reports->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $reports = $reports->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $reports as $rp ) {
            $nestedData = array ();
            $nestedData ['no'] =++$start;
            $nestedData ['nama'] = $rp->nama;
            $nestedData ['nama_rombel'] = $rp->nama_level.' - '.$rp->nama_sub_level;
            $nestedData ['jml_baca'] = $rp->riwayat_bacas->count();
            $nestedData ['tot'] = count($reports);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }
    
}
