<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Auth;

class OrtuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('ortu.list_ortu', ['menu' => 'ortu']);
    }

    public function add_ortu(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'unique:App\Models\User,email'
        ]);

        $ortu = new Ortu;
        $ortu->nama = $request->nama;
        $ortu->email = $request->email;
        $ortu->save();

        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->id_role = 3;
        $user->id_ortu = $ortu->id;
        $user->password = bcrypt('12345678');
        $user->save();

        return redirect()->route('ortu')
                        ->with('success','Berhasil menambah data OTS!');
    }

    public function edit_ortu(Request $request)
    {
        $this->validate($request, [
            'nama_edit' => 'required',
            'email_edit' => 'required',
        ]);

        $ortu = Ortu::find($request->id_ortu);
        $ortu->nama = $request->nama_edit;
        $ortu->email = $request->email_edit;
        $ortu->save();

        $user = User::where('id_ortu', $request->id_ortu)->first();
        $user->name = $request->nama_edit;
        $user->email = $request->email_edit;
        $user->save();

        return redirect()->route('ortu')
                        ->with('success','Berhasil mengubah data OTS!');
    }

    public function get_ortu_edit(Request $request)
    {
        $id = $request->id;
        $ortu = Ortu::find($id);

        return $ortu->toJson();
    }

    public function delete_ortu(Request $request)
    {
        Ortu::find($request->id)->delete();
        User::where('id_ortu', $request->id)->delete();

        return redirect()->route('ortu')
                        ->with('success','Berhasil menghapus data OTS!');
    }

    public function reset_pass(Request $request)
    {
        $user = User::where('id_ortu', $request->id)->first();
        $user->password = bcrypt('12345678');
        $user->save();

        return redirect()->route('ortu')
                        ->with('success','Berhasil reset password OTS!');
    }

    public function get_ortus(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'email',
        );
        //Getting the data
        $ortus = User::where('id_role', 3);

        $totalData = $ortus->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $ortus->where ( 'name', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'email', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $ortus->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $ortus->count ();
        // Data to client
        $jobs = $ortus->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $ortus = $ortus->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $ortus as $ots ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $ots->id_ortu;
            $nestedData ['nama'] = $ots->name;
            $nestedData ['email'] = $ots->email;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
