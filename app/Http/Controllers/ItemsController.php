<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Transaksi;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Elasticsearch\ClientBuilder;
use Artisan;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('rest.verify');
        $this->client = ClientBuilder::create()->build();
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addItem(Request $request)
    {
        $item = new Item();
        $item->nama = $request->nama;
        $item->deskripsi = $request->deskripsi;
        $item->harga = $request->harga;
        $item->stok = $request->stok;
        $item->save();

        $this->indexItem($item);

        return redirect()->route('listItem')
                        ->with('success','Barang berhasil ditambah!');
    }

    public function addItemPage() 
    {        
        return View::make('addItem');
    }

    public function editItemPage($id) 
    {
        $items = Item::find($id);
        return View::make('item.editItem', compact('items'));
    }

    public function getItem($id)
    {
        $item = Item::find($id);
        return $item;
    }

    public function viewItemPage($id) 
    {
        
    }

    public function editItem(Request $request)
    {
        $now = date('Y-m-d H:i:s');

        $item = Item::find($request->id);
        $item->nama = $request->nama;
        $item->deskripsi = $request->deskripsi;
        $item->harga = $request->harga;
        $item->stok = $request->stok;
        $item->updated_at = $now;
        $item->save();

        Artisan::call('import:data');

        return redirect()->route('listItem')
                        ->with('success','Data barang berhasil diubah');
    }

    public function beliItem(Request $request)
    {        
        $now = date('Y-m-d H:i:s');

        $items = $this->getItem($request->id_items);
        $user = User::find($request->id_user);

        $transaksi = array(
            'jenis' => 'Pembelian',
            'id_user' => $request->id_user,
            'nama_user' => $user->name,
            'id_items' => $request->id_items,
            'nama_item' => $items->nama,
            'jumlah_items' => $request->jumlah,
            'harga' => $items->harga,
            'updated_at' => $now
        );

        DB::table('transaksis')->insert($transaksi);

        $stok = $items->stok;
        $jml = $request->jumlah;

        $item = array(
            'stok' => ($stok - $jml),
            'updated_date' => $now
        );

        DB::table('items')->where('id','=',$request->id_items)->update($item);

        return redirect()->route('listShop')
                        ->with('success','Barang telah dibeli');
    }

    public function deleteItem($id)
    {
        Item::find($id)->delete();

        Artisan::call('import:data');

        return redirect()->route('listItem')
                        ->with('success','Item deleted successfully');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'deskripsi',
                3 =>'harga',
                4 =>'stok',                
        );
        //Getting the data
        $items = DB::table ( 'items' )
        ->select ( 'id', 'nama', 'deskripsi', 'harga', 'stok');           
        
        $totalData = $items->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                //$items->where ( 'nama', 'Like', '%' . $searchTerm . '%' );

                $res_elastic = $this->searchItems($searchTerm);

                $itemsArray = [];

                // If there are any movies that match given search text "hits" fill their id's in array
                if($res_elastic['hits']['total'] > 0) {

                    foreach ($res_elastic['hits']['hits'] as $hit) {
                        $itemsArray[] = $hit['_source']['id'];
                    }
                }

                // Retrieve found movies from database
                $items->whereIn('id', $itemsArray);
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $items->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $items->count ();
        // Data to client
        $jobs = $items->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $items = $items->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $items as $item ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $item->id;
            $nestedData ['nama'] = $item->nama;
            $nestedData ['deskripsi'] = $item->deskripsi;
            $nestedData ['harga'] = $item->harga;
            $nestedData ['stok'] = $item->stok;
            $nestedData ['tot'] = count($items);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    public function getBladeExcel()
    {
        $itemData = DB::table ( 'items' )
        ->select ( 'id',
            'nama',
            'deskripsi',
            'harga',
            'stok'
        )->get ();

        \Excel::create('Data Barang', function($excel) use($itemData) {

            $excel->sheet('Data Barang', function($sheet) use($itemData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Nama Barang',
                    'Deskripsi',
                    'Harga (Rp)',
                    'Stok'
                ];

                $no = 1;
                foreach ($itemData as $key => $value) {
                    $excelData[] = [
                        $no++.'',
                        $value->nama,
                        $value->deskripsi,
                        $value->harga.'',
                        $value->stok.''
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $itemData = Item::all();

        //return Response::json($itemData);

        $data = json_encode($itemData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }

    private function searchItems($text)
    {
        $params = [
            'index' => Item::ELASTIC_INDEX,
            'type' => Item::ELASTIC_TYPE,
            'body' => [
                'sort' => [
                    '_score'
                ],
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => [
                                'nama' => [
                                    'query'     => $text,
                                    'fuzziness' => '1'
                                ]
                            ]],
                            ['match' => [
                                'deskripsi' => [
                                    'query'     => $text,
                                    'fuzziness' => '0'
                                ]
                            ]]
                        ]
                    ],
                ],
            ]
        ];

        $data = $this->client->search($params);
        return $data;
    }

    private function indexItem(Item $item)
    {
        // Fill array with movie data
        $data = [
            'body' => [
                'id'            => $item->id,
                'nama'          => $item->nama,
                'deskripsi'   => $item->deskripsi,
            ],
            'index' => Item::ELASTIC_INDEX,
            'type'  => Item::ELASTIC_TYPE,
        ];

        // Send request to index new movie
        $response = $this->client->index($data);

        return $response;
    }
    
}
