<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;
use Imagick;

class BukuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function read($id, Request $request)
    {
        $from = $request->from;
        $buku = Buku::find($id);
        return view('read', ['menu' => 'read', 'buku' => $buku, 'from' => $from]);
    }

    public function katalog()
    {
        $bukus = Buku::all();
        return view('katalog', ['menu' => 'katalog', 'bukus' => $bukus]);
    }

    public function katalog_level($id_level)
    {
        $level = Level::find($id_level);

        if(Auth::user()->id_role == 3) {
            //$siswas = Auth::user()->ortu->siswa;
            $sub_levels = SubLevel::where('id_level', $id_level)->whereIn('id', Siswa::select('id_sub_level')->where('id_ortu', Auth::user()->id_ortu)->get())->get();
        } else {
            $sub_levels = $level->sub_levels;
        }

        return view('katalog', ['menu' => 'katalog', 'sub_levels' => $sub_levels]);
    }

    public function get_sub_levels_buku(Request $request)
    {
        $id_level = $request->id_level;
        $sub_levels = SubLevel::where('id_level', $id_level)->pluck('nama', 'id');

        return $sub_levels->toJson();
    }

    public function list_book()
    {
        $levels = Level::whereIn('id', ActiveLevel::select('id_level')->get())->pluck('nama', 'id');
        $levels['0'] = 'Pilih Kelas';
        $sub_levels = SubLevel::pluck('nama', 'id');
        $sub_levels['0'] = 'Pilih Sub Kelas';
        return view('list_buku', ['menu' => 'list_buku', 'levels' => $levels, 'sub_levels' => $sub_levels]);
    }

    public function add_buku(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'id_sub_level' => 'required|not_in:0',
            'file_upload' => 'required|mimes:pdf,PDF',
        ]);

        $buku = new Buku;
        $buku->judul = $request->judul;
        $buku->id_sub_level = $request->id_sub_level;
        $buku->created_by = User::find($request->id_user)->email;
        $buku->save();

        $file_data = array();
        $file_upload = $request->file('file_upload');

        if(!empty($file_upload)) {
            $jml_hal = $this->uploadFile($file_upload, $buku->id);
        }

        if(!$jml_hal) {
            $buku->delete();

            return redirect()->route('list_book')
                        ->with('danger','Gagal menyimpan buku!');    
        }

        $buku->jml_hal = $jml_hal;
        $buku->save();

        return redirect()->route('list_book')
                        ->with('success','Berhasil menyimpan buku!');
    }

    public function edit_buku(Request $request)
    {
        $this->validate($request, [
            'judul_edit' => 'required',
            'id_sub_level_edit' => 'required|not_in:0',
            'file_upload_edit' => 'mimes:pdf,PDF',
        ]);

        $buku = Buku::find($request->id_buku);
        $buku->judul = $request->judul_edit;
        $buku->id_sub_level = $request->id_sub_level_edit;
        $buku->updated_by = User::find($request->id_user_edit)->email;
        $buku->save();

        $file_data = array();
        $file_upload = $request->file('file_upload');

        if(!empty($file_upload)) {
            $jml_hal = $this->uploadFile($file_upload, $buku->id);
        
            if(!$jml_hal) {
                $buku->delete();

                return redirect()->route('list_book')
                            ->with('danger','Gagal menyimpan buku!');    
            }

            $buku->jml_hal = $jml_hal;
            $buku->save();
        }

        return redirect()->route('list_book')
                        ->with('success','Berhasil mengubah data buku!');
    }

    public function delete_buku(Request $request)
    {
        $buku = Buku::find($request->id);
        $buku->delete();

        return redirect()->route('list_book')
                        ->with('success','Berhasil menghapus buku!');
    }

    public function get_buku_edit(Request $request)
    {
        $id = $request->id;
        $buku = Buku::find($id);
        // print_r($buku);
        // die;
        $buku->id_level = $buku->sub_level->level->id;

        return $buku->toJson();
    }

    public function get_bukus(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'judul',
                2 =>'id_sub_level',
                3 =>'created_at',
                4 =>'updated_at', 
        );
        //Getting the data
        $bukus = Buku::join('sub_levels', 'sub_levels.id', 'bukus.id_sub_level')
                        ->join('levels', 'levels.id', 'sub_levels.id_level')
                        ->select('bukus.*', 'levels.nama as nama_level', 'sub_levels.nama as nama_sub_level');
        
        $totalData = $bukus->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $bukus->where ( 'judul', 'Like', '%' . $searchTerm . '%' )
                    ->orWhere( 'levels.nama', 'Like', '%' . $searchTerm . '%' )
                    ->orWhere( 'sub_levels.nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if ($request->has ( 'id_sub_level' )) {
            if ($request->input ( 'id_sub_level' ) != '') {
                $searchTerm = $request->input ( 'id_sub_level' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $bukus->where ( 'id_sub_level', '=', $searchTerm );
            }
        }
        
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $bukus->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $bukus->count ();
        // Data to client
        $jobs = $bukus->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $bukus = $bukus->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $bukus as $buku ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $buku->id;
            $nestedData ['judul'] = $buku->judul;
            $nestedData ['id_sub_level'] = $buku->id_sub_level;
            $nestedData ['nama_sub_level'] = $buku->sub_level->nama;
            $nestedData ['nama_level'] = $buku->sub_level->level->nama;
            $nestedData ['created_at'] = date_format($buku->created_at, "d-m-Y");
            $nestedData ['updated_at'] = $buku->updated_at;
            $nestedData ['tot'] = count($bukus);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    // public function get_riwayats(Request $request){
    //     // The columns variable is used for sorting
    //     $columns = array (
    //             // datatable column index => database column name
    //             0 =>'id',
    //             1 =>'tgl_baca',
    //             2 =>'judul',
    //             3 =>'created_at',
    //     );
    //     //Getting the data
    //     $riwayats = RiwayatBaca::join('bukus', 'bukus.id', '=', 'riwayat_baca.id_buku')
    //                             ->select('riwayat_baca.*', 'bukus.judul as judul');

    //     if ($request->has('id_siswa') && $request->input('id_siswa') != '') {
    //         $riwayats->where('id_siswa', $request->input('id_siswa'));
    //     }
        
    //     $totalData = $riwayats->count();            //Total record
    //     $totalFiltered = $totalData;      // No filter at first so we can assign like this
    //     // Here are the parameters sent from client for paging 
    //     $start = $request->input ( 'start' );           // Skip first start records
    //     $length = $request->input ( 'length' );   //  Get length record from start
    //     /*
    //      * Where Clause
    //      */
    //     if ($request->has ( 'search' )) {
    //         if ($request->input ( 'search.value' ) != '') {
    //             $searchTerm = $request->input ( 'search.value' );
    //             /*
    //             * Seach clause : we only allow to search on item_name field
    //             */
    //             //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
    //             $riwayats->where ( 'judul', 'Like', '%' . $searchTerm . '%' );
    //         }
    //     }

    //     /*
    //      * Order By
    //      */
    //     if ($request->has ( 'order' )) {
    //         if ($request->input ( 'order.0.column' ) != '') {
    //             $orderColumn = $request->input ( 'order.0.column' );
    //             $orderDirection = $request->input ( 'order.0.dir' );
    //             $riwayats->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
    //         }
    //     }
    //     // Get the real count after being filtered by Where Clause
    //     $totalFiltered = $riwayats->count ();
    //     // Data to client
    //     $jobs = $riwayats->skip ( $start )->take ( $length );

    //     /*
    //      * Execute the query
    //      */
    //     $riwayats = $riwayats->get();
    //     /*
    //     * We built the structure required by BootStrap datatables
    //     */
    //     $data = array ();
    //     $no = 1; 

    //     foreach ( $riwayats as $rw ) {
    //         $nestedData = array ();
    //         $nestedData ['no'] = ++$start;
    //         $nestedData ['id'] = $rw->id;
    //         $nestedData ['id_buku'] = $rw->id_buku;
    //         $nestedData ['judul'] = $rw->judul;
    //         $nestedData ['created_at'] = $rw->created_at;
    //         $nestedData ['tot'] = count($riwayats);

    //         $data [] = $nestedData;
    //     }
    //     /*
    //     * This below structure is required by Datatables
    //     */ 
    //     $tableContent = array (
    //             "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    //             "recordsTotal" => intval ( $totalData ), // total number of records
    //             "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    //             "data" => $data
    //     );
        
    //     //print_r($tableContent);

    //     return $tableContent;
    // }

    function uploadFile($file, $id_buku) {

        $name = $file->getClientOriginalName();
        $path = $file->storeAs('public/files', $name);
        $url = Storage::url($path);

        $pdf = new Pdf(public_path().$url);
        $num_pages = $pdf->getNumberOfPages();

        $destinationPath = public_path()."/buku/$id_buku/";

        if(!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true, true);
            File::makeDirectory($destinationPath.'/thumb/', 0755, true, true);
        }

        try {
            foreach (range(1, $num_pages) as $pageNumber) {
                $image_name = "page-".$pageNumber.".jpg";

                $pdf->setPage($pageNumber)
                    ->setCompressionQuality(30)
                    ->saveImage($destinationPath.$image_name);

                $img = Image::make($destinationPath.$image_name);            
                $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'thumb/'.$image_name);
            }

            Storage::delete($path);

        } catch (Exception $e) {
            return false;    
        }

        return $num_pages;
    }

    function uploadFolder(Request $request) {

        $folder = $request->folder;
        $id_sub_level = $request->id_sub_level_upload;

        foreach(Storage::files('public/uploads/'.$folder) as $filename) {
            
            $url = Storage::url($filename);
            
            $pdf = new Pdf(public_path().$url);
            $num_pages = $pdf->getNumberOfPages();

            $buku = new Buku;
            $fname = explode('/', $url)[4];
            $fs = explode('.', $fname);
            $ext = $fs[count($fs) - 1];
            $buku->judul = str_replace(".$ext", '', $fname);
            $buku->id_sub_level = $id_sub_level;
            $buku->jml_hal = $num_pages;
            $buku->save();
            $id_buku = $buku->id;

            $destinationPath = public_path()."/buku/$id_buku/";

            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0755, true, true);
                File::makeDirectory($destinationPath.'/thumb/', 0755, true, true);
            }

            try {
                foreach (range(1, $num_pages) as $pageNumber) {
                    $image_name = "page-".$pageNumber.".jpg";

                    $pdf->setPage($pageNumber)
                        ->setCompressionQuality(30)
                        ->saveImage($destinationPath.$image_name);

                    $img = Image::make($destinationPath.$image_name);            
                    $img->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'thumb/'.$image_name);
                }

                //Storage::delete($path);

            } catch (Exception $e) {
                continue;    
            }
        }

        return redirect()->route('list_book')
                        ->with('success','Berhasil upload buku!');
    }
    
}
