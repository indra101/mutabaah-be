<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Ibadah;

use Auth;
use DB;

class IbadahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('ibadah.list_ibadah', ['menu' => 'ibadah']);
    }

    public function add_ibadah(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'target' => 'required',
            'satuan' => 'required',
            'kode' => 'required',
            'icon' => 'required',
        ]);

        $ibadah = new Ibadah;
        $ibadah->nama = $request->nama;
        $ibadah->target = $request->target;
        $ibadah->satuan = $request->satuan;
        $ibadah->kode = $request->kode;
        $ibadah->icon = $request->icon;
        $ibadah->save();

        return redirect()->route('ibadah')
                        ->with('success','Berhasil menambah data ibadah!');
    }

    public function edit_ibadah(Request $request)
    {
        $this->validate($request, [
            'nama_edit' => 'required',
            'target_edit' => 'required',
            'satuan_edit' => 'required',
            'kode_edit' => 'required',
            'icon_edit' => 'required',
        ]);

        $ibadah = Ibadah::find($request->id_ibadah);
        $ibadah->nama = $request->nama_edit;
        $ibadah->target = $request->target_edit;
        $ibadah->satuan = $request->satuan_edit;
        $ibadah->kode = $request->kode_edit;
        $ibadah->icon = $request->icon_edit;
        $ibadah->save();

        return redirect()->route('ibadah')
                        ->with('success','Berhasil mengubah data ibadah!');
    }

    public function get_ibadah_edit(Request $request)
    {
        $id = $request->id;
        $ibadah = Ibadah::find($id);

        return $ibadah->toJson();
    }

    public function delete_ibadah(Request $request)
    {
        Ibadah::find($request->id)->delete();
        User::where('id_ibadah', $request->id)->delete();

        return redirect()->route('ibadah')
                        ->with('success','Berhasil menghapus data ibadah!');
    }

    public function get_ibadahs(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'target',
                3 =>'satuan',
                4 =>'icon',
                5 =>'kode',
        );
        //Getting the data
        $ibadahs = DB::table('ibadahs');

        $totalData = $ibadahs->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $ibadahs->where ( 'name', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'email', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $ibadahs->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $ibadahs->count ();
        // Data to client
        $jobs = $ibadahs->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $ibadahs = $ibadahs->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $ibadahs as $ibd ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $ibd->id;
            $nestedData ['nama'] = $ibd->nama;
            $nestedData ['target'] = $ibd->target;
            $nestedData ['satuan'] = $ibd->satuan;
            $nestedData ['icon'] = $ibd->icon;
            $nestedData ['kode'] = $ibd->kode;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
