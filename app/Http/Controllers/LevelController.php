<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;

class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $levels = Level::all();

        return view('level.list_level', ['menu' => 'level', 'levels' => $levels]);
    }

    public function add_level(Request $request)
    {
        
    }

    public function edit_level(Request $request)
    {

    }

    public function delete_level(Request $request)
    {
        
    }

    public function set_status(Request $request)
    {
        $id_level = $request->id;
        $active_level = ActiveLevel::where('id_level', $id_level)->get();

        if($active_level->isEmpty()) {
            $new_level = new ActiveLevel;
            $new_level->id_level = $id_level;
            $new_level->save();
            $pesan = 'Berhasil mengaktifkan Kelas';
        } else {
            ActiveLevel::destroy($active_level[0]->id);
            $pesan = 'Berhasil menon-aktifkan Kelas';
        }

        $res = array('status' => true);

        //return json_encode($res);
        return redirect()->route('level')
                        ->with('success', $pesan);
    }

    public function get_levels(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'tgl_baca',
                2 =>'judul',
                3 =>'created_at',
        );
        //Getting the data
        $levels = Level::select();

        $totalData = $levels->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $levels->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $levels->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $levels->count ();
        // Data to client
        $jobs = $levels->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $levels = $levels->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        $active_levels = ActiveLevel::pluck('id', 'id_level');

        foreach ( $levels as $lvl ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $lvl->id;
            $nestedData ['nama'] = $lvl->nama;
            $nestedData ['sub_level'] = $lvl->sub_levels;
            $nestedData ['status'] = (isset($active_levels[$lvl->id])) ? 1 : 0;
            $nestedData ['tot'] = count($levels);

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
