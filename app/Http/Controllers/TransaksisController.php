<?php

namespace App\Http\Controllers;

use App\Transaksi;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class TransaksisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */    

    public function getTransaksi($id)
    {
        $transaksi = Transaksi::find($id);
        return $transaksi;
    }

    public function getTransaksiByUser($id)
    {
        $transaksi = Transaksi::where('id_user', $id)->get();
        return $transaksi;
    }

    public function viewTransaksiPage($id) 
    {
        
    }

    public function deleteTransaksi($id)
    {
        Transaksi::find($id)->delete();
        return redirect()->route('listTransaksi')
                        ->with('success','Transaksi deleted successfully');
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'jenis',
                2 =>'nama_item',
                3 =>'nama_user',
                4 =>'harga',
                5 =>'jumlah_items',
                6 =>'created_date',
        );
        //Getting the data
        $transaksis = DB::table ( 'transaksis' )
        ->select ( '*');
        
        $totalData = $transaksis->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on transaksi_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $transaksis->where ( 'nama_item', 'Like', '%' . $searchTerm . '%' )
                            ->orWhere ( 'nama_user', 'Like', '%' . $searchTerm . '%' );
            }
        }

        if(session('role_id') != 1) {
            $transaksis->where ( 'id_user', session('user_id') );
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $transaksis->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $transaksis->count ();
        // Data to client
        $jobs = $transaksis->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $transaksis = $transaksis->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $transaksis as $transaksi ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $transaksi->id;
            $nestedData ['jenis'] = $transaksi->jenis;
            $nestedData ['id_user'] = $transaksi->id_user;
            $nestedData ['id_items'] = $transaksi->id_items;
            $nestedData ['jumlah_items'] = $transaksi->jumlah_items;
            $nestedData ['nama_user'] = $transaksi->nama_user;
            $nestedData ['nama_item'] = $transaksi->nama_item;
            $nestedData ['harga'] = $transaksi->harga;
            $nestedData ['created_date'] = $transaksi->created_date;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );

        return $tableContent;
    }

    public function getBladeExcel()
    {
        $transaksiData = DB::table ( 'transaksis' )
        ->select('*')->get();

        \Excel::create('Riwayat Transaksi', function($excel) use($transaksiData) {

            $excel->sheet('Riwayat Transaksi', function($sheet) use($transaksiData) {

                $excelData = [];
                $excelData[] = [
                    'No',
                    'Jenis',
                    'Nama User',
                    'Nama Barang',
                    'Harga (Rp)',
                    'Jumlah',
                    'Tanggal'
                ];

                $no = 1;
                foreach ($transaksiData as $key => $value) {
                    $excelData[] = [
                        $no++,
                        $value->jenis,
                        $value->nama_user,
                        $value->nama_item,
                        $value->harga,
                        $value->jumlah_items,
                        $value->created_date
                    ];                    
                }

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download('xlsx');

    }

    public function getJson()
    {
        $transaksiData = Transaksi::all();

        //return Response::json($transaksiData);

        $data = json_encode($transaksiData);
        $file = time() . '_file.json';
        $destinationPath=public_path()."/upload/json/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
        return response()->download($destinationPath.$file);

    }
    
}
