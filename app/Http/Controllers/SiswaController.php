<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;

use Auth;

class SiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $levels = Level::pluck('nama', 'id');
        $levels['0'] = 'Pilih Kelas';
        $sub_levels = SubLevel::pluck('nama', 'id');
        $sub_levels['0'] = 'Pilih Sub Kelas';

        $ortus = Ortu::pluck('nama', 'id');
        $ortus['0'] = 'Pilih Orang Tua';

        return view('siswa.list_siswa', ['menu' => 'siswa', 'levels' => $levels, 'ortus' => $ortus]);
    }

    public function add_siswa(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
        ]);

        $siswa = new Siswa;
        $siswa->nama = $request->nama;
        $siswa->panggilan = $request->panggilan;
        $siswa->id_ortu = $request->id_ortu;
        $siswa->id_sub_level = $request->id_sub_level;
        $siswa->created_by = Auth::user()->id;
        $siswa->save();

        return redirect()->route('siswa')
                        ->with('success','Berhasil menambah data siswa!');
    }

    public function edit_siswa(Request $request)
    {
        $this->validate($request, [
            'nama_edit' => 'required',
        ]);

        // echo $request->panggilan;
        // die;

        $siswa = Siswa::find($request->id_siswa);
        $siswa->nama = $request->nama_edit;
        $siswa->id_sub_level = $request->id_sub_level_edit;
        $siswa->panggilan = $request->panggilan;
        $siswa->id_ortu = $request->id_ortu_edit;
        $siswa->save();

        return redirect()->route('siswa')
                        ->with('success','Berhasil mengubah data siswa!');
    }

    public function get_siswa_edit(Request $request)
    {
        $id = $request->id;
        $siswa = Siswa::find($id);
        $siswa->id_level = $siswa->sub_level->level->id;

        return $siswa->toJson();
    }

    public function delete_siswa(Request $request)
    {
        $siswa = Siswa::find($request->id);
        $siswa->delete();

        return redirect()->route('siswa')
                        ->with('success','Berhasil menghapus data siswa!');
    }

    public function get_siswas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'nama_level',
                3 =>'nama_sub_level',
                4 =>'panggilan',
                5 =>'nama_ortu',
        );
        //Getting the data
        $siswas = Siswa::select('siswas.*', 'levels.nama as nama_level', 'sub_levels.nama as nama_sub_level', 'ortus.nama as nama_ortu')
                        ->join('sub_levels', 'sub_levels.id', 'siswas.id_sub_level')
                        ->join('levels', 'levels.id', 'sub_levels.id_level')
                        ->join('ortus', 'ortus.id', 'siswas.id_ortu')
                        ;

        $totalData = $siswas->count();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $siswas->where ( 'siswas.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'levels.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'sub_levels.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'ortus.nama', 'Like', '%' . $searchTerm . '%' )
                        ->orWhere ( 'siswas.panggilan', 'Like', '%' . $searchTerm . '%' )
                        ;
            }
        }

        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $siswas->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $siswas->count ();
        // Data to client
        $jobs = $siswas->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $siswas = $siswas->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1; 

        foreach ( $siswas as $ssw ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $ssw->id;
            $nestedData ['nama'] = $ssw->nama;
            $nestedData ['nama_level'] = $ssw->nama_level;
            $nestedData ['nama_sub_level'] = $ssw->nama_sub_level;
            $nestedData ['panggilan'] = $ssw->panggilan;
            $nestedData ['nama_ortu'] = $ssw->nama_ortu;

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

}
