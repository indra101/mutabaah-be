<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Setting;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class RestMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = new \GuzzleHttp\Client();
        //$username = Session::get('success')

        // $data = $request->session()->all();

        // print_r($data);
        // die;

        //$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L3BvYy1sa3BwLTIwMjEvcHVibGljL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjEzNDg1MjkwLCJleHAiOjE2MTM0ODg4OTAsIm5iZiI6MTYxMzQ4NTI5MCwianRpIjoiS2tTaURsblBVZFdnandjMyIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.1sglNjcJPOmNv_FLHMbbh2HcfUuIJ01jFH-cNEJ1NVs';
        $token = session('token');
        $url_verify = Setting::first()->url_verify;
        
        $response = $client->request('GET', $url_verify, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]
        ]);
        $response = json_decode($response->getBody()->getContents());
        // echo '<pre>';
        // print_r($response);
        // die;

        if(!empty($response->success)) {
            if($response->success) {
                return $next($request);

            } else {
                $request->session()->flush();
                return redirect()->route('login')->with('error', 'Silahkan Login');    
            }
        } else {

            $request->session()->flush();
            return redirect()->route('login')->with('error', 'Silahkan Login');
        }
    }
}


