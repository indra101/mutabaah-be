<?php

namespace App\Api\V1\Controllers;

use App\Models\Aktivitas;
use App\Models\Pekan;
use App\Models\User;
use App\Models\Ibadah;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class AktivitassController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */    

    public function getAktivitas($id)
    {
        $aktivitas = Aktivitas::find($id);
        return Response::json($aktivitas);
    }

    public function getAktivitasByUser($id)
    {
        $aktivitas = Aktivitas::where('id_user', $id)->orderBy('id', 'desc')->get();
        $ibadah = Ibadah::all();
        $target = 0;
        
        foreach($ibadah as $ibd) {
            $target += $ibd->target;
        }

        $i = 0;
        foreach($aktivitas as $akt) {

            $achive = $this->countAchive($ibadah, $akt);

            $aktivitas[$i]->pekan = $akt->pekan;
            $aktivitas[$i]->target = $target;
            $aktivitas[$i]->persen = $achive;
            $aktivitas[$i]->persen_round = round($achive*100, 0);
            $aktivitas[$i]->nomor = count($aktivitas) - $i;
            $i++;
        }

        $data = array('data' => $aktivitas);
        return Response::json($data);
    }

    public function getRiwayatAktivitasByUser($id)
    {
        // get list skip record 1
        $aktivitas = Aktivitas::where('id_user', $id)->orderBy('id', 'desc')->skip(1)->take(PHP_INT_MAX)->get();
        $ibadah = Ibadah::all();
        $target = 0;
        
        foreach($ibadah as $ibd) {
            $target += $ibd->target;
        }

        $i = 0;
        foreach($aktivitas as $akt) {

            $achive = $this->countAchive($ibadah, $akt);

            $aktivitas[$i]->pekan = $akt->pekan;
            $aktivitas[$i]->target = $target;
            $aktivitas[$i]->persen = $achive;
            $aktivitas[$i]->persen_round = round($achive*100, 0);
            $aktivitas[$i]->nomor = count($aktivitas) - $i;
            $i++;
        }

        $data = array('data' => $aktivitas);
        return Response::json($data);
    }

    public function countAchive($ibadah, $aktivitas) {
        //$ibadah = Ibadah::all();

        $subuh = $aktivitas->subuh / $ibadah[0]->target;
        $tilawah = $aktivitas->tilawah / $ibadah[1]->target;
        $tahajjud = $aktivitas->tahajjud / $ibadah[2]->target;
        $infaq = $aktivitas->infaq / $ibadah[3]->target;
        $matsurat = $aktivitas->matsurat / $ibadah[4]->target;
        $olahraga = $aktivitas->olahraga / $ibadah[5]->target;

        $subuh = ($subuh > 1) ? 1 : $subuh;
        $tilawah = ($tilawah > 1) ? 1 : $tilawah;
        $tahajjud = ($tahajjud > 1) ? 1 : $tahajjud;
        $infaq = ($infaq > 1) ? 1 : $infaq;
        $matsurat = ($matsurat > 1) ? 1 : $matsurat;
        $olahraga = ($olahraga > 1) ? 1 : $olahraga;

        $achive = ($subuh + $tilawah + $tahajjud + $infaq + $matsurat + $olahraga) / 6;

        // echo "achive: $achive";
        // die;

        return $achive;
    }

    public function getAktivitasByUserPekan(Request $request)
    {
        $id_user = $request->id_user;
        $id_pekan = $request->id_pekan;

        $aktivitas = Aktivitas::where('id_user', $id_user)->where('id_pekan', $id_pekan)->get();
        $data = array('data' => $aktivitas);
        return Response::json($data);
    }

    public function getCurrAktivitasByUser($id)
    {
        $now = date('Y-m-d');
        $id_pekan = Pekan::where("tgl_start", '<=', $now)->where("tgl_end", '>=', $now)->first()->id;
        $aktivitas = Aktivitas::where('id_user', $id)->where('id_pekan', $id_pekan)->first();
        if(empty($aktivitas)) {
            $new_akt = new Aktivitas();
            $new_akt->id_user = $id;
            $new_akt->id_pekan = $id_pekan;
            $new_akt->save();
            $aktivitas = $new_akt;
            $aktivitas->pekan = $new_akt->pekan;
        }
        $aktivitas->pekan = $aktivitas->pekan;
        $ibadah = Ibadah::all();
        $achive = $this->countAchive($ibadah, $aktivitas);
        $aktivitas->persen = $achive;
        $aktivitas->persen_round = round($achive*100, 0);

        $data = array('data' => $aktivitas);
        return Response::json($data);
    }

    public function getListAktivitas($id_user, $id_pekan)
    {
        $aktivitas = Aktivitas::where('id_user', $id)->where('id_pekan', $id_pekan)->get();
        return Response::json($aktivitas);
    }

    public function viewAktivitasPage($id) 
    {
        
    }

    public function getAllAktivitas(){
        $aktivitas = Aktivitas::all();
        $data = array('data' => $aktivitas);
        return Response::json($data);
    }

    public function saveAktivitas(Request $request) 
    {
        $id = $request->id;
        $id_user = $request->id_user;
        $id_pekan = $request->id_pekan;

        if($id) {
            $akt = Aktivitas::find($id);
        } else {
            $akt = new Aktivitas;
            $akt->id_user = $id_user;
            $akt->id_pekan = $id_pekan;
        }

        $akt->subuh = $request->subuh;
        $akt->tilawah = $request->tilawah;
        $akt->tahajjud = $request->tahajjud;
        $akt->infaq = $request->infaq;
        $akt->matsurat = $request->matsurat;
        $akt->olahraga = $request->olahraga;
        $akt->save();

        $ibadah = Ibadah::all();
        $achive = $this->countAchive($ibadah, $akt);
        $akt->persen = $achive;
        $akt->persen_round = round($achive*100, 0);
        $akt->pekan = $akt->pekan;

        // print_r($akt);
        // die;

        $data = array('data' => $akt);
        return Response::json($data);
    }

    public function deleteAktivitas($id)
    {
        Aktivitas::find($id)->delete();
        return Response::json([
            'success' => 1]);
    }
    
}
