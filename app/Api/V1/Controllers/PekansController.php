<?php

namespace App\Api\V1\Controllers;

use App\Models\Pekan;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class PekansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */    

    public function getPekan($id)
    {
        $pekan = Pekan::find($id);
        return Response::json($pekan);
    }

    public function getPekanByUser($id)
    {
        $pekan = Pekan::where('id_user', $id)->get();
        return Response::json($pekan);
    }

    public function viewPekanPage($id) 
    {
        
    }

    public function getAllPekan(){
        $pekan = Pekan::all();
        return Response::json($pekan);
    }

    public function deletePekan($id)
    {
        Pekan::find($id)->delete();
        return Response::json([
            'success' => 1]);
    }
    
}
