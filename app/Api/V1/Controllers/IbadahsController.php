<?php

namespace App\Api\V1\Controllers;

use App\Models\Ibadah;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class IbadahsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */    

    public function getIbadah($id)
    {
        $ibadah = Ibadah::find($id);
        return Response::json($ibadah);
    }

    public function getIbadahByUser($id)
    {
        $ibadah = Ibadah::where('id', $id)->get();
        $data = array('data' => $ibadah);
        return Response::json($data);
    }

    public function viewIbadahPage($id) 
    {
        
    }

    public function getAllIbadah(){
        $ibadah = Ibadah::all();
        $data = array('data' => $ibadah);
        //return Response::json($ibadah);
        return Response::json($data);
    }

    public function deleteIbadah($id)
    {
        Ibadah::find($id)->delete();
        return Response::json([
            'success' => 1]);
    }
    
}
