<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
//use Tymon\JWTAuth\JWTAuth;
use JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;

class LoginController extends Controller
{
    //public function login(LoginRequest $request, JWTAuth $JWTAuth)
    public function login2(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = $JWTAuth->attempt($credentials);

            if(!$token) {
                throw new AccessDeniedHttpException();
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        return response()
            ->json([
                'status' => 'ok',
                'token' => $token,
                'user' => $JWTAuth->toUser($token)
            ]);
    }

    public function login(Request $request)
    {
        // $field = filter_var($request->get('username'), FILTER_VALIDATE_EMAIL)
        //     ? 'email'
        //     : 'username';

        // $rules = [
        //     $field => 'required',
        //     'password' => 'required',
        // ];

        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ];

        //$credentials = array($field => $request->get('username'), 'password' => $request->get('password'));
        $credentials_check = array('username' => $request->get('username'), 'email' => $request->get('email'), 'password' => $request->get('password'));
        $credentials = array('username' => $request->get('username'), 'password' => $request->get('password'));
        
        $validator = Validator::make($credentials_check, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials.'], 401);
                throw new AccessDeniedHttpException();
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
            throw new HttpException(500);
        }
        // all good so return the token
        //$user = User::where($field, $request->username)->first();
        $user = User::where('username', $request->username)->first();
        $user->email = $request->email;
        $user->save();
        $user->access_token = $token;
        return response()->json(['success' => true, 'data'=> [ 'token' => $token, 'user' => $user, 'expires' => JWTAuth::factory()->getTTL() * 60, ]]);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->header( 'Authorization' );
        try {
            JWTAuth::parseToken()->invalidate( $token );

            return response()->json( [
                'success' => true,
                'error'   => false,
                'message' => trans( 'auth.logged_out' )
            ] );
        } catch ( TokenExpiredException $exception ) {
            return response()->json( [
                'success' => false,
                'error'   => true,
                'message' => trans( 'auth.token.expired' )

            ], 401 );
        } catch ( TokenInvalidException $exception ) {
            return response()->json( [
                'success' => false,
                'error'   => true,
                'message' => trans( 'auth.token.invalid' )
            ], 401 );

        } catch ( JWTException $exception ) {
            return response()->json( [
                'success' => false,
                'error'   => true,
                'message' => trans( 'auth.token.missing' )
            ], 500 );
        }

        // return response()->json(['success' => true, 'message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function verify()
    {
        return response()->json(['success' => true]);
    }
}
