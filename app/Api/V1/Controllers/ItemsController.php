<?php

namespace App\Api\V1\Controllers;

use App\Item;
use App\Transaksi;
use App\Models\User;
use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function addItem(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'stok' => $request->stok
        );

        DB::table('items')->insert($data);

        return Response::json($data);
    }

    public function getDatas(Request $request){
        // The columns variable is used for sorting
        $columns = array (
                // datatable column index => database column name
                0 =>'id',
                1 =>'nama',
                2 =>'deskripsi',
                3 =>'harga',
                4 =>'stok',                
        );
        //Getting the data
        $items = DB::table ( 'items' )
        ->select ( 'id', 'nama', 'deskripsi', 'harga', 'stok');           
        
        $totalData = $items->count ();            //Total record
        $totalFiltered = $totalData;      // No filter at first so we can assign like this
        // Here are the parameters sent from client for paging 
        $start = $request->input ( 'start' );           // Skip first start records
        $length = $request->input ( 'length' );   //  Get length record from start
        /*
         * Where Clause
         */
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $searchTerm = $request->input ( 'search.value' );
                /*
                * Seach clause : we only allow to search on item_name field
                */
                //$candidates->where ( 'users.name', 'Like', '%' . $searchTerm . '%' );
                $items->where ( 'nama', 'Like', '%' . $searchTerm . '%' );
            }
        }
        /*
         * Order By
         */
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $items->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }
        // Get the real count after being filtered by Where Clause
        $totalFiltered = $items->count ();
        // Data to client
        $jobs = $items->skip ( $start )->take ( $length );

        /*
         * Execute the query
         */
        $items = $items->get();
        /*
        * We built the structure required by BootStrap datatables
        */
        $data = array ();
        $no = 1;
        foreach ( $items as $item ) {
            $nestedData = array ();
            $nestedData ['no'] = ++$start;
            $nestedData ['id'] = $item->id;
            $nestedData ['nama'] = $item->nama;
            $nestedData ['deskripsi'] = $item->deskripsi;
            $nestedData ['harga'] = $item->harga;
            $nestedData ['stok'] = $item->stok;            

            $data [] = $nestedData;
        }
        /*
        * This below structure is required by Datatables
        */ 
        $tableContent = array (
                "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => intval ( $totalData ), // total number of records
                "recordsFiltered" => intval ( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data" => $data
        );
        
        //print_r($tableContent);

        return $tableContent;
    }

    public function getItem($id)
    {
        $item = Item::find($id);
        return Response::json($item);
    }

    public function getItem2($id)
    {
        $item = Item::find($id);
        return $item;
    }

    public function editItem(Request $request)
    {
        $data = array(
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'stok' => $request->stok
        );

        DB::table('items')->where('id','=',$request->id)->update($data);

        return Response::json($data);
    }

    public function deleteItem($id)
    {
        Item::find($id)->delete();

        //return $this->response->noContent();
        return Response::json([
            'status' => 'ok']);
    }

    public function getAllItem()
    {
        $itemData = Item::all();

        return Response::json($itemData);

    }

    public function beliItem(Request $request)
    {        
        $now = date('Y-m-d H:i:s');

        $items = $this->getItem2($request->id_items);
        $user = User::find($request->id_user);
        $jumlah = $request->jumlah_items;

        $transaksi = array(
            'jenis' => 'Pembelian',
            'id_user' => $request->id_user,
            'nama_user' => $user->name,
            'id_items' => $request->id_items,
            'nama_item' => $items->nama,
            'jumlah_items' => $jumlah,
            'harga' => $items->harga,
            //'created_date' => $now
            'created_at' => $now
        );

        DB::table('transaksis')->insert($transaksi);

        $stok = $items->stok;
        $jml = $jumlah;

        $item = array(
            'stok' => ($stok - $jml),
            'updated_date' => $now
        );

        DB::table('items')->where('id','=',$request->id_items)->update($item);

        return Response::json($transaksi);
    }

    private function searchItems($text)
    {
        $params = [
            'index' => Item::ELASTIC_INDEX,
            'type' => Item::ELASTIC_TYPE,
            'body' => [
                'sort' => [
                    '_score'
                ],
                'query' => [
                    'bool' => [
                        'should' => [
                            ['match' => [
                                'nama' => [
                                    'query'     => $text,
                                    'fuzziness' => '1'
                                ]
                            ]]
                        ]
                    ],
                ],
            ]
        ];

        $data = $this->client->search($params);
        return $data;
    }

}
