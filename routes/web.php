<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/kontak', 'HomeController@kontak')->name('kontak');

Route::get('/read/{id}', ['as'=>'read','uses'=>'BukuController@read']);

Route::get('/katalog', 'BukuController@katalog')->name('katalog');
Route::get('/list_book', 'BukuController@list_book')->name('list_book');
Route::get('/get_bukus', 'BukuController@get_bukus')->name('get_bukus');
Route::post('/add_buku', 'BukuController@add_buku')->name('add_buku');
Route::get('/convert', 'BukuController@convert')->name('convert');
Route::get('/katalog_level/{id}', 'BukuController@katalog_level')->name('katalog_level');
Route::get('/get_sub_levels_buku', 'BukuController@get_sub_levels_buku')->name('get_sub_levels_buku');
Route::get('/get_buku_edit', 'BukuController@get_buku_edit')->name('get_buku_edit');
Route::post('/edit_buku', 'BukuController@edit_buku')->name('edit_buku');
Route::post('/delete_buku', 'BukuController@delete_buku')->name('delete_buku');

Route::get('/laporan/{id}', 'RiwayatController@laporan')->name('laporan');
Route::post('/add_riwayat', 'RiwayatController@add_riwayat')->name('add_riwayat');
Route::get('/get_riwayats', 'RiwayatController@get_riwayats')->name('get_riwayats');
Route::get('/riwayat_baca', 'RiwayatController@riwayat_baca')->name('riwayat_baca');
Route::get('/edit_riwayat_page/{id}', 'RiwayatController@edit_riwayat_page')->name('edit_riwayat_page');
Route::post('/edit_riwayat', 'RiwayatController@edit_riwayat')->name('edit_riwayat');
Route::post('/delete_riwayat', 'RiwayatController@delete_riwayat')->name('delete_riwayat');

Route::get('/level', 'LevelController@index')->name('level');
Route::get('/get_levels', 'LevelController@get_levels')->name('get_levels');
Route::get('/set_status', 'LevelController@set_status')->name('set_status');

Route::get('/sub_level/{id}', 'SubLevelController@index')->name('sub_level');
Route::get('/get_sub_levels', 'SubLevelController@get_sub_levels')->name('get_sub_levels');
Route::post('/add_sub_level', 'SubLevelController@add_sub_level')->name('add_sub_level');
Route::post('/delete_sub_level', 'SubLevelController@delete')->name('delete_sub_level');
Route::post('/edit_sub_level', 'SubLevelController@edit_sub_level')->name('edit_sub_level');

Route::get('/siswa', 'SiswaController@index')->name('siswa');
Route::get('/get_siswas', 'SiswaController@get_siswas')->name('get_siswas');
Route::post('/add_siswa', 'SiswaController@add_siswa')->name('add_siswa');
Route::get('/get_siswa_edit', 'SiswaController@get_siswa_edit')->name('get_siswa_edit');
Route::post('/edit_siswa', 'SiswaController@edit_siswa')->name('edit_siswa');
Route::post('/delete_siswa', 'SiswaController@delete_siswa')->name('delete_siswa');

Route::get('/ortu', 'OrtuController@index')->name('ortu');
Route::get('/get_ortus', 'OrtuController@get_ortus')->name('get_ortus');
Route::post('/add_ortu', 'OrtuController@add_ortu')->name('add_ortu');
Route::get('/get_ortu_edit', 'OrtuController@get_ortu_edit')->name('get_ortu_edit');
Route::post('/edit_ortu', 'OrtuController@edit_ortu')->name('edit_ortu');
Route::post('/delete_ortu', 'OrtuController@delete_ortu')->name('delete_ortu');
//Route::post('/reset_pass', 'OrtuController@reset_pass')->name('reset_pass');

Route::get('/report', 'ReportController@index')->name('report');
Route::get('/get_reports', 'ReportController@get_reports')->name('get_reports');

Route::post('/uploadFolder', 'BukuController@uploadFolder')->name('uploadFolder');

Route::get('/profile', 'UserController@index')->name('profile');
Route::post('/edit_profil', 'UserController@edit_profil')->name('edit_profil');

// User -----------------------------------------------------------------------------------

Route::get('/users', 'UsersController@index')->name('users');
Route::get('addUserPage', function () {return view('addUser');});
Route::get('addPage',['as'=>'add.page','uses'=>'UsersController@addUserPage']);
Route::get('editUserPage/{id}',['as'=>'edit.page','uses'=>'UsersController@editUserPage']);
Route::get('viewUserPage/{id}',['as'=>'view.page','uses'=>'UsersController@viewUserPage']);
Route::get('delUser/{id}', ['as'=>'delete.User','uses'=>'UsersController@deleteUser']);
Route::get('get-users', ['as'=>'get.data','uses'=>'UsersController@getData']);
Route::get('get-users1', ['as'=>'get-users1','uses'=>'UsersController@getDatas']);
Route::get('get-excel', ['as'=>'get.excel','uses'=>'UsersController@getBladeExcel']);
Route::get('get-json', ['as'=>'get.json','uses'=>'UsersController@getJson']);
Route::post('add_user', ['as'=>'add.User','uses'=>'UsersController@addUser']);
Route::post('edit_user', ['as'=>'edit.User','uses'=>'UsersController@editUser']);
Route::post('/reset_pass', 'UsersController@reset_pass')->name('reset_pass');

Route::get('/ibadah', 'IbadahController@index')->name('ibadah');
Route::get('/get_ibadahs', 'IbadahController@get_ibadahs')->name('get_ibadahs');
Route::post('/add_ibadah', 'IbadahController@add_ibadah')->name('add_ibadah');
Route::get('/get_ibadah_edit', 'IbadahController@get_ibadah_edit')->name('get_ibadah_edit');
Route::post('/edit_ibadah', 'IbadahController@edit_ibadah')->name('edit_ibadah');
Route::post('/delete_ibadah', 'IbadahController@delete_ibadah')->name('delete_ibadah');

Route::get('/aktivitas', 'AktivitasController@index')->name('aktivitas');
Route::get('/get_aktivitass', 'AktivitasController@get_aktivitass')->name('get_aktivitass');
Route::post('/add_aktivitas', 'AktivitasController@add_aktivitas')->name('add_aktivitas');
Route::get('/get_aktivitas_edit', 'AktivitasController@get_aktivitas_edit')->name('get_aktivitas_edit');
Route::post('/edit_aktivitas', 'AktivitasController@edit_aktivitas')->name('edit_aktivitas');
Route::post('/delete_aktivitas', 'AktivitasController@delete_aktivitas')->name('delete_aktivitas');

Route::get('/pekan', 'PekanController@index')->name('pekan');
Route::get('/get_pekans', 'PekanController@get_pekans')->name('get_pekans');
Route::post('/add_pekan', 'PekanController@add_pekan')->name('add_pekan');
Route::get('/get_pekan_edit', 'PekanController@get_pekan_edit')->name('get_pekan_edit');
Route::post('/edit_pekan', 'PekanController@edit_pekan')->name('edit_pekan');
Route::post('/delete_pekan', 'PekanController@delete_pekan')->name('delete_pekan');

Route::get('/unit', 'UnitController@index')->name('unit');
Route::get('/get_units', 'UnitController@get_units')->name('get_units');
Route::post('/add_unit', 'UnitController@add_unit')->name('add_unit');
Route::get('/get_unit_edit', 'UnitController@get_unit_edit')->name('get_unit_edit');
Route::post('/edit_unit', 'UnitController@edit_unit')->name('edit_unit');
Route::post('/delete_unit', 'UnitController@delete_unit')->name('delete_unit');