<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;
use App\Http\Requests;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
  $api->group(['prefix' => 'auth'], function(Router $api) {
    $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
    $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
    $api->post('login2', 'App\\Api\\V1\\Controllers\\LoginController@login2');
    $api->post('register', 'App\\Api\\V1\\Controllers\\ApiController@addUser');

    $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
    $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
  });

  $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
    $api->get('protected', function() {
      return response()->json([
                  'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
      ]);
    });

  });

  //$api->group(['middleware' => 'jwt.verify'], function ($api) {
  	$api->get('getAllUser', 'App\Api\V1\Controllers\ApiController@getAllUser');
    $api->get('getUser/{id}', 'App\Api\V1\Controllers\ApiController@getUser');
    $api->get('getUserByEmail/{email}', 'App\Api\V1\Controllers\ApiController@getUserByEmail');
  	$api->post('addUser', 'App\Api\V1\Controllers\ApiController@addUser');
  	$api->post('editUser', 'App\Api\V1\Controllers\ApiController@editUser');
  	$api->delete('deleteUser/{id}', 'App\Api\V1\Controllers\ApiController@deleteUser');
  //});

    $api->get('getAllItem', 'App\Api\V1\Controllers\ItemsController@getAllItem');
  	$api->get('getItem/{id}', 'App\Api\V1\Controllers\ItemsController@getItem');
  	$api->post('addItem', 'App\Api\V1\Controllers\ItemsController@addItem');
  	$api->post('editItem', 'App\Api\V1\Controllers\ItemsController@editItem');
  	$api->delete('deleteItem/{id}', 'App\Api\V1\Controllers\ItemsController@deleteItem');
    $api->post('beliItem', 'App\Api\V1\Controllers\ItemsController@beliItem');

    $api->get('getTransaksiByUser/{id}', 'App\Api\V1\Controllers\TransaksisController@getTransaksiByUser');

    $api->get('getAllIbadah', 'App\Api\V1\Controllers\IbadahsController@getAllIbadah');
  	$api->get('getIbadah/{id}', 'App\Api\V1\Controllers\IbadahsController@getIbadah');
    $api->get('getIbadahByUser/{id}', 'App\Api\V1\Controllers\IbadahsController@getIbadahByUser');
  	$api->post('addIbadah', 'App\Api\V1\Controllers\IbadahsController@addIbadah');
  	$api->post('editIbadah', 'App\Api\V1\Controllers\IbadahsController@editIbadah');
  	$api->delete('deleteIbadah/{id}', 'App\Api\V1\Controllers\IbadahsController@deleteIbadah');

    $api->get('getAllPekan', 'App\Api\V1\Controllers\PekansController@getAllPekan');
  	$api->get('getPekan/{id}', 'App\Api\V1\Controllers\PekansController@getPekan');
  	$api->post('addPekan', 'App\Api\V1\Controllers\PekansController@addPekan');
  	$api->post('editPekan', 'App\Api\V1\Controllers\PekansController@editPekan');
  	$api->delete('deletePekan/{id}', 'App\Api\V1\Controllers\PekansController@deletePekan');

    $api->get('getAllAktivitas', 'App\Api\V1\Controllers\AktivitassController@getAllAktivitas');
  	$api->get('getAktivitas/{id}', 'App\Api\V1\Controllers\AktivitassController@getAktivitas');
    $api->get('getAktivitasByUser/{id}', 'App\Api\V1\Controllers\AktivitassController@getAktivitasByUser');
    $api->get('getRiwayatAktivitasByUser/{id}', 'App\Api\V1\Controllers\AktivitassController@getRiwayatAktivitasByUser');
    $api->get('getCurrAktivitasByUser/{id}', 'App\Api\V1\Controllers\AktivitassController@getCurrAktivitasByUser');
  	$api->post('getAktivitasByUserPekan', 'App\Api\V1\Controllers\AktivitassController@getAktivitasByUserPekan');
    $api->post('addAktivitas', 'App\Api\V1\Controllers\AktivitassController@addAktivitas');
    $api->post('saveAktivitas', 'App\Api\V1\Controllers\AktivitassController@saveAktivitas');
  	$api->post('editAktivitas', 'App\Api\V1\Controllers\AktivitassController@editAktivitas');
  	$api->delete('deleteAktivitas/{id}', 'App\Api\V1\Controllers\AktivitassController@deleteAktivitas');

    $api->get('verify', 'App\\Api\\V1\\Controllers\\LoginController@verify');
    $api->post('logout', 'App\\Api\\V1\\Controllers\\LoginController@logout');
  //});

  $api->get('getAllDataUser', 'App\Api\V1\Controllers\ApiController@getAllUser');
  $api->get('getAllDataItem', 'App\Api\V1\Controllers\ItemsController@getAllItem');
  $api->get('getAllDataTransaksi', 'App\Api\V1\Controllers\TransaksisController@getAllTransaksi');

  $api->get('refresh', function(Request $Request) {
    $input=$Request->all();
        $token = $input['Token'];

   if(!$token){
    $Err['status']='error';
    $Err['msg']='There is no token';
    return response()
    ->json($Err, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE| JSON_PRETTY_PRINT);

    }

    try{
        $token = JWTAuth::refresh($token);

  }catch (JWTException $e) {
    $ERR['status']='error';
    $ERR['MSG']= "the was erorr on you token ";
    return response()
    ->json($ERR, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE| JSON_PRETTY_PRINT);

            }

     $Sucss['status']='success';
     $Sucss['token']= $token;
     return response()
    ->json($Sucss, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE| JSON_PRETTY_PRINT);

      });

  $api->get('hello', function() {
    return response()->json([
                'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
    ]);
  });
});

/*Route::group(['middleware' => 'jwt.auth'], function()
{
    Route::get('getAllUser', 'ApiController@getAllUser');
});*/
